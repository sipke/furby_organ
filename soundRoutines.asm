//ADSR 00F8
//VIBR 83 10
//WAVE 11

initSound:
	ldx #0
	lda #0
!:	sta SID,x	
	inx	
	cpx #24	
	bne !-	
	lda #$0f
	sta SID + 24
	rts	

// possible 4 sound events: on, vibrato, start release, off (hard restart)
// and sequence is always the same, so we just need  the number of frames till the next event, and current step of course  
currentSoundEventQueue0:
	.byte 0,0,0,0
currentSoundEventQueue1:
	.byte 0,0,0,0
currentSoundEventQueue2:
	.byte 0,0,0,0
currentSoundStep:
	.byte 2,2,2

attackDecay:
	.byte 0,0,0
sustain:
	.byte 0,0,0
release:
	.byte 0,0,0

freqLo:
	.byte 0,0,0
freqHi:
	.byte 0,0,0

// TODO: now limited to 255 notes, is not much	
//                                          ------------ song ----------------------------

.import source "sidwizard_import.asm"

/*
instrumentTable0:
	.byte 1,1,1,0,1
	.byte 1,1,1,0,1
lengthTable0:
	.byte 24,24,18,18,12
	.byte 24,24,18,18,12
noteTable0:
	//.byte 4,8,11,13,14
	//.byte 4,8,11,13,14
	.byte 0,4,7,9,10
	.byte 0,4,7,9,10

instrumentTable1:
	.byte 2
	.byte 2
	.byte 2,1,1,1,0,1
	.byte 2,1,1,1,0,1
lengthTable1:
	.byte 32*6
	.byte 32*6
	.byte 12,18,18,18,18,12
	.byte 12,18,18,18,18,12
noteTable1:
	.byte 26
	.byte 26
	//.byte 26,26,25,23,22,23
	//.byte 22,22,21,20,19,20
	.byte 22,22,21,19,18,19
	.byte 18,18,17,16,15,16
	
instrumentTable2:
	.byte 2
	.byte 2
	.byte 2,1,1,1,0,1
	.byte 2,1,1,1,0,1
lengthTable2:
	.byte 32*6
	.byte 32*6
	.byte 12,18,18,18,18,12
	.byte 12,18,18,18,18,12
noteTable2:
	.byte 26
	.byte 26
	.byte 22+12,22+12,21+12,19+12,18+12,19+12
	.byte 18+12,18+12,17+12,16+12,15+12,160+12
//	.byte 26+12,26+12,25+12,23+12,22+12,23+12
	//.byte 22+12,22+12,21+12,20+12,19+12,20+12

nrEvents:
	.byte lengthTable0-instrumentTable0,lengthTable1-instrumentTable1,lengthTable2-instrumentTable2
	.print "nr events: " + (lengthTable1-instrumentTable1)

*/


placeInSong:
	.byte $ff,$ff,$ff
	
//----------------------------------------------------------------------------------------------	
 
	
jumpTable:
	.word setInstrument0, setInstrument1, setNone

.assert "jumpTable should be at least 6 bytes below page boundary", true, floor(jumpTable/256) == floor((jumpTable+6)/256)

doVoice0:
	playStep(0)
doVoice1:
	playStep(1)
doVoice2:
	playStep(2)


.macro playStep(index) {	
	ldx currentSoundStep + index	
	lda currentSoundEventQueue0 + 4*index,x	//length left in step
	beq doNextStep	
	sec
	sbc #$01	
	sta currentSoundEventQueue0 + 4*index,x	
	jmp end
doNextStep:	
	inx	
	stx currentSoundStep + index	
	cpx #3	
	beq newNote	
!:	cpx #2
	bne !+
	lda #$10	//start release
	sta SID + 4 +index*7
	lda #0	
	sta SID + 5 + index*7	
	sta SID + 6 + index*7	
	lda #SLEEP_FRAME_0	
	sta currentFrame + index	
	jmp done
!:	cpx #1	//set do vibrato 		
	bne done		
	lda #1
	sta doVib + index
done:
	rts

newNote:

	lda #0
	sta currentSoundStep + index
	
	ldx placeInSong + index
	inx
	cpx nrEvents + index
	bne !+
	ldx #0
!:	stx placeInSong + index

	
	.if(index ==0) {
		lda instrumentTable0,x
		asl
		clc
		adc #<jumpTable
		sta jmpTo + 1
		lda lengthTable0,x
		sta lengthTmp
		lda noteTable0,x
		sta activeFurby + index
	}
	.if(index ==1) {
		lda instrumentTable1,x
		asl
		clc
		adc #<jumpTable
		sta jmpTo + 1
		lda lengthTable1,x
		sta lengthTmp
		lda noteTable1,x
		sta activeFurby + index
	}
	.if(index ==2) {
		lda instrumentTable2,x
		asl
		clc
		adc #<jumpTable
		sta jmpTo + 1
		lda lengthTable2,x
		sta lengthTmp
		lda noteTable2,x
		sta activeFurby + index
	}
	ldx #index*7
	ldy #index
//debug check
	lda jmpTo + 1
	cmp #<jumpTable+6
	bcc jmpTo
	break()
	nop
//---------	
jmpTo:	
	jmp (jumpTable)	
	
end:	
	rts	
}	
tmpinstr:	.byte 0
tmpattacklength:	.byte 0
tmpattacklengthquart:	.byte 0
tmpattacklengthhalf:	.byte 0
tmpattacklengththreequart:	.byte 0

tmpdecaylength:	.byte 0
tmpdecaylengthhalf:	.byte 0

tmpreleaselength:	.byte 0
tmpreleaselengthquart:	.byte 0
tmpreleaselengthhalf:	.byte 0
tmpreleaselengththreequart:	.byte 0



setNone:
	sty voiceTmp	
	lda #$0	
	sta SID + 5,x	
	sta SID + 6,x	

	lda #$10	
	sta SID + 4,x	

	ldx voiceTmp		//index 0,1,2	
	lda #0	
	sta doVib,x			// don't do vibrato yet
	// soundstep 3 = hard restart	
	lda #2
	sta currentSoundStep,x

	ldx voiceTmp
	txa		//index times 4 to get index to eventqueue (is 4 long for each voice)
	asl
	asl
	tax

	// only length of last event (hard restart)
	lda lengthTmp
	sta currentSoundEventQueue0 + 2,x
	
	lax voiceTmp		//index * 64 is start of table for that voice
	asl
	asl
	asl
	asl
	asl
	asl
	adc #0	//n-1 animation comprises of n frames
	tay
	
	lda #SLEEP_FRAME_0
	sta animationQueue0,y
	lda lengthTmp
	sta lengthQueue0,y
	dey

	lda #1		//n 
	sta animationQueuePosition,x
	lda #0
	sta animationLengthLeft,x

	rts

/* x containing voice offset = 0,7, 14*/
/* y containing voice = 0,1,2 */
setInstrument0:	
	sty voiceTmp	
	lda #$00	
	sta SID + 5,x	
	lda #$f8	
	sta SID + 6,x	

	lda activeFurby,y	
	clc	
	adc #FURBY_INDEX_TO_NOTE_OFFSET	
	sta currentNote,y	

	tay	
	lda FreqTablePalHi,y
	sta SID + 1, x
	sta freqHiTmp
	lda FreqTablePalLo,y
	sta SID, x 	
	sta freqLoTmp
	
	lda #$11	
	sta SID + 4,x	

	ldx voiceTmp		//index 0,1,2	
	lda #0	
	sta doVib,x			// don't do vibrato yet
	
	lda #6				
	sta vibWidth,x	
	lsr	
	sta vibCount,x	//start halfway 	
	
	// soundstep 0 = attack decay phase	
	lda #0
	sta currentSoundStep,x

	lda freqLoTmp
	sta freqLo,x
	lda freqHiTmp
	sta freqHi,x

	ldy #8	//vibratowidth
	lda FreqTablePalHi,y
	sta vibratoValueHi, x
	lda FreqTablePalLo,y
	sta vibratoValueLo, x		

	ldx voiceTmp
	txa		//index times 4 to get index to eventqueue (is 4 long for each voice)
	asl
	asl
	tax

// TODO do something for when length not long enough for all events	

	lda lengthTmp
	cmp #FRAMES_TILL_VIBRATO + 2
	bcs timeEnough
	lda #2
	sta currentSoundEventQueue0,x
	lda #1
	sta currentSoundEventQueue0 + 1,x
	lda lengthTmp
	sec
	sbc #3
	jmp setRelease	

timeEnough:	
	lda #FRAMES_TILL_VIBRATO
	sta currentSoundEventQueue0 ,x
	lda lengthTmp
	sec
	sbc #FRAMES_TILL_VIBRATO + 2
setRelease:	
	sta currentSoundEventQueue0 + 1,x
	lda #2
	sta currentSoundEventQueue0 + 2,x


	
	lax voiceTmp		//index * 64 is start of table for that voice
	asl
	asl
	asl
	asl
	asl
	asl
	adc #4	//n-1 animation comprises of n frames
	tay
	
	lda #SUSTAIN_LEVEL_0
	sta animationQueue0,y
	lda #1
	sta lengthQueue0,y
	dey

	lda #SUSTAIN_LEVEL_1
	sta animationQueue0,y
	lda #1
	sta lengthQueue0,y

	dey
	lda #SUSTAIN_LEVEL_2
	sta animationQueue0,y
	lda #1
	sta lengthQueue0,y

	dey
	lda #SUSTAIN_LEVEL_3
	sta animationQueue0,y
	lda lengthTmp
	sec
	sbc #5
	sta lengthQueue0,y
	
	dey
	lda #NEUTRAL_FRAME
	sta animationQueue0,y
	lda #2
	sta lengthQueue0,y


	lda #5		//n 
	sta animationQueuePosition,x
	lda #0
	sta animationLengthLeft,x

	
	rts

setInstrument1:	
/* x containing voice offset = 0,7, 14*/
/* y containing voice = 0,1,2 */
	sty voiceTmp	
	lda #$0c	
	sta SID + 5,x	
	lda #$00	
	sta SID + 6,x	

	lda activeFurby,y	
	clc	
	adc #FURBY_INDEX_TO_NOTE_OFFSET	
	sta currentNote,y	

	tay	
	lda FreqTablePalHi,y
	sta SID + 1, x
	sta freqHiTmp
	lda FreqTablePalLo,y
	sta SID, x 	
	sta freqLoTmp
	
	lda #$11	
	sta SID + 4,x	

	ldx voiceTmp		//index 0,1,2	
	lda #0	
	sta doVib,x			// don't do vibrato yet
	
	lda #6				
	sta vibWidth,x	
	lsr	
	sta vibCount,x	//start halfway 	
	
	// soundstep 0 = attack decay phase	
	lda #0
	sta currentSoundStep,x

	lda freqLoTmp
	sta freqLo,x
	lda freqHiTmp
	sta freqHi,x

	ldy #8	//vibratowidth
	lda FreqTablePalHi,y
	sta vibratoValueHi, x
	lda FreqTablePalLo,y
	sta vibratoValueLo, x		

	ldx voiceTmp
	txa		//index times 4 to get index to eventqueue (is 4 long for each voice)
	asl
	asl
	tax

// TODO do something for when length not long enough for all events	

	lda #1 //Attack decay length
	sta currentSoundEventQueue0,x
	lda #1 //FRAMES_TILL_VIBRATO
	sta currentSoundEventQueue0 + 1,x
	lda lengthTmp
	sec
	sbc #4
	sta currentSoundEventQueue0 + 2,x
	lda #2
	sta currentSoundEventQueue0 + 3,x
	
	lax voiceTmp		//index * 64 is start of table for that voice
	asl
	asl
	asl
	asl
	asl
	asl
	adc #2	//n-1 animation comprises of n frames
	tay
	
	lda #SUSTAIN_LEVEL_1
	sta animationQueue0,y
	lda #1
	sta lengthQueue0,y
	dey

	dey
	lda #SUSTAIN_LEVEL_3
	sta animationQueue0,y
	lda lengthTmp
	sec
	sbc #3
	sta lengthQueue0,y
	
	dey
	lda #NEUTRAL_FRAME
	sta animationQueue0,y
	lda #2
	sta lengthQueue0,y


	lda #3		//n 
	sta animationQueuePosition,x
	lda #0
	sta animationLengthLeft,x

	
	rts



// 1 frame = 1/50 or 1/60 sec = 20 or ~16 ms	
			//	0		1		2		3		4		5		6		7		8		9		A		B		C		D		E		F	
			//	2,  	8,	   16, 	   24,	   38,	   56,	   68,	   80,	  100,	  250,	  500,	  800,   1000,   3000,   5000,   8000
.var als = List().add(2,  	8,	   16, 	   24,	   38,	   56,	   68,	   80,	  100,	  250,	  500,	  800,   1000,   3000,   5000,   8000)			
attackFrameCount:			
	//.fill 16, ceil(als.get(i)/16)		
	.byte 1,1,1,2,3,4,5,5,7,16,32,50,63,188,313,500		
			// decay, release
attackLengthPerFrame:
	.byte 0,0,0,1
	.byte 0,0,0,1
	.byte 0,0,0,1
	.byte 0,1,0,1
	.byte 1,1,0,1
	.byte 1,1,1,1
	.byte 1,1,1,2
	.byte 1,1,1,2
	.byte 2,2,1,2
	.byte 4,4,4,4
	.byte 8,8,8,8
	.byte 12,13,12,13
	.byte 16,15,16,16
	.byte 44,45,44,45
	.byte 79,78,78,78
	.byte 125,125,125,125
.var dls = List().add(6,      24,     48,     72,    114,    168,    204,    240,    300,    750,   1500,   2400,   3000,   9000,  15000,  24000) 			
decayFrameCount: 
releaseFrameCount: 
	//.fill 16, ceil(dls.get(i)/16)
	.byte 1,2,3,5,8,11,13,15,19,47,94,150,188,563,938,1500 		
 			// 0        1       3       4      6        9      10      12      15      38      75     120     150     450     750    1200
decayLengthPerFrame:
	.byte 0,0,0,1
	.byte 0,1,0,1
	.byte 0,1,1,1
	.byte 1,1,1,2
	.byte 2,2,2,2
	.byte 2,3,3,3
	.byte 3,3,3,4
	.byte 4,4,3,4
	.byte 4,5,5,5
	.byte 12,11,12,12
	.byte 23,24,23,24
	.byte 38,37,37,38
	//------------ really any lengths adding up to 256+ will lead to problems  
	//----- shouldn't use any decay >9   	
	.byte 44,45,44,45
	.byte 141,141,140,141
	.byte 234,235,234,235  // 
	.byte 255,255,255,255 // actually should be 375,375,375,375 but that's ridicully long, should nog be used



	
voiceTmp:
	.byte 0
lengthTmp:	
	.byte 0	
freqLoTmp:
	.byte 0
freqHiTmp:
	.byte 0
vibFreqLoTmp:
	.byte 0
vibFreqHiTmp:
	.byte 0

vibratoValueLo:
	.byte 0
vibratoValueHi:
	.byte 0

vibFreqLo:
	.byte 0,0,0
vibFreqHi:
	.byte 0,0,0
vibWidth: 	
	.byte 0,0,0	
vibCount:	
	.byte 0,0,0	
doVib:	
	.byte 0,0,0	


FreqTablePalLo:
	        //      C   C#  D   D#  E   F   F#  G   G#  A   A#  B
                .byte $17,$27,$39,$4b,$5f,$74,$8a,$a1,$ba,$d4,$f0,$0e  // 1
                .byte $2d,$4e,$71,$96,$be,$e8,$14,$43,$74,$a9,$e1,$1c  // 2
                .byte $5a,$9c,$e2,$2d,$7c,$cf,$28,$85,$e8,$52,$c1,$37  // 3
                .byte $b4,$39,$c5,$5a,$f7,$9e,$4f,$0a,$d1,$a3,$82,$6e  // 4
                .byte $68,$71,$8a,$b3,$ee,$3c,$9e,$15,$a2,$46,$04,$dc  // 5
                .byte $d0,$e2,$14,$67,$dd,$79,$3c,$29,$44,$8d,$08,$b8  // 6
                .byte $a1,$c5,$28,$cd,$ba,$f1,$78,$53,$87,$1a,$10,$71  // 7
                .byte $42,$89,$4f,$9b,$74,$e2,$f0,$a6,$0e,$33,$20,$ff  // 8

FreqTablePalHi:
		//      C   C#  D   D#  E   F   F#  G   G#  A   A#  B
                .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$02  // 1
                .byte $02,$02,$02,$02,$02,$02,$03,$03,$03,$03,$03,$04  // 2
                .byte $04,$04,$04,$05,$05,$05,$06,$06,$06,$07,$07,$08  // 3
                .byte $08,$09,$09,$0a,$0a,$0b,$0c,$0d,$0d,$0e,$0f,$10  // 4
                .byte $11,$12,$13,$14,$15,$17,$18,$1a,$1b,$1d,$1f,$20  // 5
                .byte $22,$24,$27,$29,$2b,$2e,$31,$34,$37,$3a,$3e,$41  // 6
                .byte $45,$49,$4e,$52,$57,$5c,$62,$68,$6e,$75,$7c,$83  // 7
                .byte $8b,$93,$9c,$a5,$af,$b9,$c4,$d0,$dd,$ea,$f8,$ff  // 8

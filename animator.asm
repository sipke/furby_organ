
//========================= ANIMATOR =========================

.macro animateNew(index) {	
	ldx animationQueuePosition + index
check:	
	lda frameLengths + 9*index,x
	bne decrease
	inx
	// this compare is here to prevent 'catastrophic' values of x 
	// but it's only here because somewhere else the code is crappy and I didn't want to debug anymore 	
	cpx animationNrFrames + index
	bcc !+
	jmp end
!:	jmp check	
decrease:	
	dec frameLengths + 9*index,x	
	bne end	
	inx	
// as a precaution		
//	cpx #9
//	bcc !+
//	break()
//	ldx #8
//!:	
	stx  animationQueuePosition + index	
	
	lda frames + 9*index,x
//.eval brkFile.writeln("break " + toHexString(*) + " if A > 10")
	sta currentFrame + index
	sta baseFrame + index
end:	
}	                                        
//======================== pre music animation handle
.macro doPreSong(index) {
	ldx activeFurby + index
	bmi setNewAnim
	inc animCount + index
	lda animationQueuePosition + index
	cmp animationNrFrames + index
// if end of current animation is reached
//start new animation	
	bcc end
	// set active furby to non active
	lda #0
	sta furbyActive,x
	//dec furbyActive,x
	lda #$80
	sta activeFurby + index

//filter off for voice		
	lda $d417
	and #255-(pow(2,index))		
	sta $d417		


	jmp setNewAnim
end:
	rts
setNewAnim:	
//take pseudo random number	
	 jsr getRandom
     and #$01  
     sta animType + index
     bne !+

     ldx #index
     ldy #index*9
     jsr setAnimationRange0
     jmp setFurbyToAnimate

     ldx #index
     ldy #index*9
     jsr setAnimationRange1
//-------------------------------------------
setFurbyToAnimate:
	lda #0
	sta animCount + index
	lda randomSeed
range:	 
	cmp #47
	bcc !+
	lsr
	jmp range
!:	
	sta activeFurby + index    
	tax   
	lda #1
	sta furbyActive,x
	//inc furbyActive,x
	rts
}
randomSeed:
	.byte floor(random()*256)//$1d

setAnimationRange0:	
	lda #0		//set to first frame
	sta animationQueuePosition,x
	sta animCount,x
	//write frames and lengths
	lda #YAWN_FRAME_0
	sta frames,y
	//make first current frame
	sta currentFrame,x
	sta baseFrame,y
	lda #YAWN_FRAME_1
	sta frames + 1,y 
	lda #YAWN_FRAME_2
	sta frames+ 2,y
	lda #YAWN_FRAME_3
	sta frames + 3,y
	lda #YAWN_FRAME_2
	sta frames + 4,y
	lda #YAWN_FRAME_1
	sta frames + 5,y
	lda #YAWN_FRAME_0
	sta frames + 6,y
	lda #SLEEPY_FRAME
	sta frames + 7,y
	lda #SLEEP_FRAME_0
	sta frames + 8,y

	lda #4
	sta frameLengths,y
	lda #4
	sta frameLengths + 1,y 
	lda #8
	sta frameLengths + 2,y
	lda #16
	sta frameLengths + 3,y
	lda #8
	sta frameLengths + 4,y
	lda #4
	sta frameLengths + 5,y
	lda #4
	sta frameLengths + 6,y
	lda randomSeed
	ora #128
	sta frameLengths + 7,y
	//lsr
	
	clc
	adc #$01	//should never be zero
	sta frameLengths + 8,y

	lda #9	
	sta animationNrFrames,x	
	rts	

setAnimationRange1:	
	lda #0		//set to first frame
	sta animationQueuePosition,x
	sta animCount,x
	//write frames and lengths
	lda #SLEEP_FRAME_0
	sta frames,y
	//make first current frame
	sta currentFrame,x
	sta baseFrame,y
	lda #SLEEP_FRAME_1
	sta frames + 1,y 
	lda #SLEEP_FRAME_0
	sta frames+ 2,y
	lda #SLEEP_FRAME_1
	sta frames + 3,y
	lda #SLEEP_FRAME_0
	sta frames + 4,y
	lda #SLEEP_FRAME_1
	sta frames + 5,y
	lda #SLEEP_FRAME_0
	sta frames + 6,y
	lda #SLEEPY_FRAME
	sta frames + 7,y
	lda #SLEEP_FRAME_0
	sta frames + 8,y

	lda #8
	sta frameLengths,y
	lda #8
	sta frameLengths + 1,y 
	lda #8
	sta frameLengths + 2,y
	lda #8
	sta frameLengths + 3,y
	lda #8
	sta frameLengths + 4,y
	lda #8
	sta frameLengths + 5,y
	lda #8
	sta frameLengths + 6,y
	lda randomSeed
	ora #128
	sta frameLengths + 7,y
	//lsr
	clc
	adc #$01	//should never be zero
	sta frameLengths + 8,y

	lda #9	
	sta animationNrFrames,x	
	rts		
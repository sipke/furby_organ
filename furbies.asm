
.const PERF = false

.const SCROLL_TEXT_OFFSET = 206

//default value of YSCROLL of $d011 = 2, so bad lines at raster >=$30 <=$f7 where raster = 2 mod 8
.const NO_VIBRATO = $ff
.const MAX_FRAME = 9
.const BEGIN_LINE = 51
.const IRQ_LINE= 40
.const SPRITE_MEM_START = $2500 
  
//.const CHAR_SET_START = $3800 
.const NUM_SPRITES = 16 
.const MAX_CHAR = 100	
.const SCROLL_LINE = 11
.const SPRITE_START = SPRITE_MEM_START/64

.const NEUTRAL_SPRITE = SPRITE_START + 0

.const FURB_COLOR_ZP_START = $02 
.const FURB_FRAME_ZP_START =FURB_COLOR_ZP_START + $40  
.const ZP_TMP0 = FURB_FRAME_ZP_START+ $40 
.const ZP_TMP1 = FURB_FRAME_ZP_START+ $40 + 1 
.const ZP_TMP2 = FURB_FRAME_ZP_START+ $40 + 2 
.const ZP_TMP3 = FURB_FRAME_ZP_START+ $40 + 3 

.const ZP_INSTR_0 = FURB_COLOR_ZP_START
.const ZP_INSTR_1 = FURB_COLOR_ZP_START + 2
.const ZP_INSTR_2 = FURB_COLOR_ZP_START + 4

.const ZP_LENGTH_0 = FURB_COLOR_ZP_START + 6
.const ZP_LENGTH_1 = FURB_COLOR_ZP_START + 8
.const ZP_LENGTH_2 = FURB_COLOR_ZP_START + 10

.const ZP_NOTE_0 = FURB_COLOR_ZP_START + 12
.const ZP_NOTE_1 = FURB_COLOR_ZP_START + 14
.const ZP_NOTE_2 = FURB_COLOR_ZP_START + 16

// at $90 starts KERNAL ZP
.const ZP_FREQ_0 = ZP_TMP3 +1
.const ZP_FREQ_1 = ZP_TMP3 +3
.const ZP_FREQ_2 = ZP_TMP3 +5
.assert "zp not overlap kernal zp", true, ZP_FREQ_2 + 1 < $90

.const RESTART_MUSIC_AT_NOTE = List().add(3,4,3)

.const BACKGROUND_COLOR =0

.const SCREEN_START = $0400

.const FIRST_CHAR_POS= $0403

.const NEUTRAL_FRAME = 0
.const SLEEP_FRAME_0 = 3
.const SLEEP_FRAME_1 = 2
.const SLEEPY_FRAME = 1

.const YAWN_FRAME_0  = 3
.const YAWN_FRAME_1  = 2
.const YAWN_FRAME_2  = 9
.const YAWN_FRAME_3  = 10

.const SUSTAIN_LEVEL_0 = 4 
.const SUSTAIN_LEVEL_1 = 5 
.const SUSTAIN_LEVEL_2 = 6 
.const SUSTAIN_LEVEL_3 = 7 

.const SUSTAIN_LEVEL_4 = 8 

.const FRAMES_TILL_VIBRATO = 30

.const STATE_INTRO = 0
.const STATE_TRANSITION = 2
.const STATE_MUSIC = 1

.const NR_OF_OF_ANIMS_BEFORE_MUSIC = 96

.const SID = $d400
.const FURBY_INDEX_TO_NOTE_OFFSET = 24//36
.const TRANSPOSE_SID_WIZARD_AMOUNT = 5// 0 for christmas song


.const MAX_ANIMATION_STEP_LENGTH =64

.const DATA_MEM_START = $4000

.const MAX_FURBY = $2f

.const D011_BASE = %00011011 
.const D011_24 = D011_BASE 
.const D011_25 = D011_24 | %00001000 
.var GHOST_BYTE
.if((D011_BASE & %01000000) == %01000000) {
	.eval GHOST_BYTE = $39ff
	}else { 
	.eval GHOST_BYTE = $3fff
} 

//======================== declare (table) variables =========================
.var lengthTable0
.var instrumentTable0
.var noteTable0
.var lengthTable1
.var instrumentTable1
.var noteTable1
.var lengthTable2
.var instrumentTable2
.var noteTable2
.var colorList
//=====================================================================================  
.var brkFile = createFile("debug.txt") 
.eval brkFile.writeln("break load 4000 cfff if 1==35")

.import source "colors.asm"
.import source "vibrato.asm"
.import source "assortedMacros.asm"
.import source "scroller.asm"

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

.macro breakOnStore(address) {
    .eval brkFile.writeln("break store" + toHexString(address))
}

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 "setup" 
	.import source "setup.asm"

.memblock "interrupt"
//-------------------------     interrupt	
int:		
	sei
			pha        //store register A in stack
			txa
			pha        //store register X in stack
			tya
			pha 	 //store register Y in stack
	lda #%00111011
	sta $d015			

	//sprite 6 x pos never changes, so do now, saves precious cycles later on
	lda #48 + 24*8
	sta $d00c
	
	.for (var line = 0; line<8; line ++) {

// a bit before being drawn but after previous sprites have beeen drawn, set pointers and colors
	lda #BEGIN_LINE + line *24 - 3
!:	cmp $d012
	bne !-
	//inc $d020

	// black keyes
	.if(mod(line,2)==0) {
		lda #%10111011
		sta $d015			
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 1
		sta 2040 			
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 3
		sta 2041 			
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 6
		sta 2043 			
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 8
		sta 2044 			
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 10
		sta 2045 			

		
		lda #colorPairs.get((floor((7-line)/2)*12) + 1).c1//floor(colorListDark1.get(random()*colorListDark1.size()))
		sta $d027 + 0		
		lda #colorPairs.get((floor((7-line)/2)*12) + 3).c1//floor(colorListDark1.get(random()*colorListDark1.size()))
		sta $d027 + 1		
		lda #colorPairs.get((floor((7-line)/2)*12) + 6).c1//floor(colorListDark1.get(random()*colorListDark1.size()))
		sta $d027 + 3
		lda #colorPairs.get((floor((7-line)/2)*12) + 8).c1//floor(colorListDark1.get(random()*colorListDark1.size()))
		sta $d027 + 4
		lda #colorPairs.get((floor((7-line)/2)*12) + 9).c1//floor(colorListDark1.get(random()*colorListDark1.size()))
		sta $d027 + 5

	lda #48 + 2*8
	sta $d000
	lda #48 + 6*8
	sta $d002
	lda #48 + 14*8
	sta $d006
	lda #48 + 18*8
	sta $d008
	lda #48 + 22*8
	sta $d00a

	}else {
		lda #%11111111
		sta $d015
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 0
		sta 2040 
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 2
		sta 2041 
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 4
		sta 2042 
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 5
		sta 2043 
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 7
		sta 2044 
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 9
		sta 2045 
		lda FURB_FRAME_ZP_START + floor((7-line)/2)*12 + 11
		sta 2046 
//6 + 7*7 = 56 cycles
		lda #colorPairs.get((floor((7-line)/2)*12) + 0).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 0
		lda #colorPairs.get((floor((7-line)/2)*12) + 2).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 1
		lda #colorPairs.get((floor((7-line)/2)*12) + 4).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 2
		lda #colorPairs.get((floor((7-line)/2)*12) + 5).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 3
		lda #colorPairs.get((floor((7-line)/2)*12) + 7).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 4
		lda #colorPairs.get((floor((7-line)/2)*12) + 9).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 5
		lda #colorPairs.get((floor((7-line)/2)*12) + 11).c1//floor(colorListLight1.get(random()*colorListLight1.size()))
		sta $d027 + 6
//7*6 = 42 cycles
	lda #48 
	sta $d000
	lda #48 + 4*8
	sta $d002
	lda #48 + 8*8
	sta $d004
	lda #48 + 12*8
	sta $d006
	lda #48 + 16*8
	sta $d008
	lda #48 + 20*8
	sta $d00a

	}

	// now wait until being drawn				
	// somewhere inbetween is a badline, but we don't need to worry about that
	lda #BEGIN_LINE + 1 + line *24
!:	cmp $d012
	bne !-
	//inc $d020
//set y position of next batch of furbies, unless we're already at the eight line

.if(line !=7) {
	lda #BEGIN_LINE + (line+1) *24
} else {
	lda #SCROLL_LINE 
	sta $d005
	sta $d00d
	sta $d00f
}
.if(mod(line,2)==0) {
	sta $d001
	sta $d003
	sta $d005
	sta $d007
	sta $d009
	sta $d00b
	sta $d00d

} else {
	sta $d001
	sta $d003
	sta $d007
	sta $d009
	sta $d00b

}
	//dec $d020
	//dec $d020

 // at this point we have time to spare to do other things, like update bottom scroller and set characters on top of furbies 
 // at least 16 lines which is loads of time	
	.if(mod(line,2)==0 && line <6) {
		lda state
		cmp #STATE_MUSIC
		beq !+
		jmp noVibrato 
!:		doVibrato(floor(line/2))
		jmp *+6
noVibrato:		
		.if(floor(line/2)==0) {	jsr handlePreSong0}		
		.if(floor(line/2)==1) {	jsr handlePreSong1}		
		.if(floor(line/2)==2) {	jsr handlePreSong2}		
	}

	.if(line==1) {
		jsr doVoice0
	}
	.if(line==3) {
		jsr doVoice1
	}
	.if(line==5) {
		jsr doVoice2
	}
	.if (line ==6){
		calcvx()
	}
	.if(line==7) {
		calcvy()
	}


	scrollMacro(line)
	lda #0
	sta $d020
 
}	

//-------------- draw scroll sprites and open border
	// open border
	lda $d011
	tay	//25 row
	and #%11110111
	tax	//24 row
	lda #$fa
!:	cmp $d012
	bne !-
	stx $d011
	lda #$ff
!:	cmp $d012
	bne !-
	sty $d011
//-----------------------------------
//set X positions and Sprite Char prioritization (if state is music)
{
	lda state
	cmp #STATE_MUSIC
	beq !+
	ldx #0
	jmp ontop
!:	
	ldx scrollCharColorCycleIndex
	inx
	cpx #scrollCharColorCycleIndex-scrollCharColorCycle
	bne !+
	ldx #0
!:	stx scrollCharColorCycleIndex
	cpx #14
	bcc ontop
	cpx #scrollCharColorCycleIndex-scrollCharColorCycle-5
	bcs ontop
beneath:	
	lda #$ff
	sta $d01b
	jmp d01bdone
ontop:	
	lda #$00
	sta $d01b
d01bdone:
	lda scrollCharColorCycle,x
	.for(var i=0; i<8;i++) { 
		sta $d027+i
		ldx #((scrollSpriteMemStart/64) + i)  
		stx 2040+i
	}
	ldx #0
	lda scrollLineStartX
	clc
!:	sta $d000,x
	adc #48
	bcc carryClear
	
	stx nrSpriteWithin255
	clc 
carryClear:
	inx
	inx
	cpx #16
	bne !-
	lda nrSpriteWithin255
	lsr
	tay
	iny
	lda msbXTable,y
	//lda #%11000000

	sta $d010
}
//update scroll sprite position 
	dec scrollLineStartX
	bpl !+
 	ldx #15
 	stx scrollLineStartX
 	lda #1
 	sta doLargeScroll
!:

	lda #255	//expand horizontally
	sta $d01d
	sta $d015	//enable all
	
	lda #0	//MC
	sta $d01c 

	stabilizeRaster()

	delay(63-12)

	ldx #BLUE//ORANGE
	stx $d021
	
	lda ghostByteTable + 0
	sta GHOST_BYTE
	.for(var i=0;i<8;i++){
		sta GHOST_BYTE
	}
	
	delay(19)
	lda ghostByteTable + 1
//--------------------------

	sta GHOST_BYTE
	.for(var i=0;i<8;i++){
		sta GHOST_BYTE
	}
	stx $d021
	lda ghostByteTable + 2
//-------------------------	
	sta GHOST_BYTE
	.for(var i=0;i<8;i++){
		sta GHOST_BYTE
	}
	stx $d021
	
	lda ghostByteTable + 3
	.for(var i=0;i<6;i++) {	
		sta GHOST_BYTE
		.for(var j=0;j<8;j++){
			sta GHOST_BYTE
		}
		.if(i!=5) {
			sta GHOST_BYTE
			stx $d021
			lda ghostByteTable + 4+i
		}
	}		
	delay(21)
	lda #0
	sta GHOST_BYTE
	lda #BACKGROUND_COLOR
	sta $d021

	.for(var i=11; i>=0;i--) {
		lda ghostByteTable + i
		sta ghostByteTable + i + 1 
	}
	lda ghostByteTable + 12
	sta ghostByteTable 


//turns sprites off, only turn them on at start of next interrupt
	lda #0
	sta $d015

	lda #0
	sta $d01d	//expand X
	lda #%01111111	//MC
	sta $d01b
	sta $d01c
	 
//--------------------------------------------
//reset X positions for first line which is a black key line
	
	lda #48 + 2*8
	sta $d000
	lda #48 + 6*8
	sta $d001
	lda #48 + 14*8
	sta $d003
	lda #48 + 18*8
	sta $d004
	lda #48 + 22*8
	sta $d005

	lda #%10000000
	sta $d010
//set first y pos of sprites
	lda #BEGIN_LINE 
	sta $d001
	sta $d003
	sta $d005
	sta $d007
	sta $d009
	sta $d00b
	sta $d00d
	

.for (var i=0; i<3;i++) {
	ldx activeFurby + i
	bmi dontAnimate
.if(PERF==true) {inc $d021}
	lda currentFrame + i
	
	jsr copySpriteChars
 	
	ldx activeFurby + i
	lda currentFrame + i	
	clc	
	adc	#(SPRITE_MEM_START/64)
	sta FURB_FRAME_ZP_START,x	
	animateNew(i)	
.if(PERF==true) {dec $d021}		
dontAnimate:		
}
	calcPos()
	lda #(orbitalSprite/64)
	sta 2047
	ldx #GREY
	lda $d01b
	and #%01111111
	ldy d01b
	bne !+
	ldx #DARK_GREY
	ora #%10000000
!:	sta $d01b
	stx $d02e

	lda gotoNextState
	beq !+ 
	lda #STATE_MUSIC
	sta state
!:

		
//for determing how many cycles are left
//	lda $d012	
//	cmp maxd012
//	bcc !+
//	sta maxd012
//!:

// looks like we're at line 302 max, so 10 lines left on screen minus 1 for ending the interrupt
// plus lines we can use at the beginning, maybe add one more animation? 
endIrq:			
	lda #<int														//				2
	 sta $fffe														// 				6
	 lda #>int													//					8								
	 sta $ffff        					//											12


	lda $d011
	and #%01111111
	sta $d011
	lda IRQ_LINE
	sta $D012

			
	lsr $d019			
	pla
	tay        //restore register Y from stack (remember stack is FIFO: First In First Out)
	pla
	tax        //restore register X from stack
	pla        //restore register A from stack
	lda $dc01
	and #%00010000
	bne !+
	lda #$37
	sta $01
	jmp ($FFFC)	
	cli		
!:	rti		

.memblock "tables"
.import source "orbit.asm"

ghostByteTable:
	.byte %00011000
	.byte %01100000
	.byte %11000000
	.byte %10000001
	.byte %11000000
	.byte %01100000
	.byte %00011000
	.byte %00000110
	.byte %00000011
	.byte %10000001
	.byte %00000011
	.byte %00000110
	.byte %00011000
	
framesTillNextEvent:
	.fill 3,0	
	
doLargeScroll:	.byte 0			//indicates if a byte value scroll is needed	
nrSpriteWithin255:
		.byte 0
scrollLineStartX:
		.byte 15
msbXTable:		
		.byte %11111111, %11111110, %11111100, %11111000, %11110000, %11100000, %11000000, %10000000, %00000000		

furbyCharColors:
	.for(var i=0;i<48;i++){
		.byte colorPairs.get(i).c2//
	}
scrollCharColorCycle:  
/*
	.fill 9, WHITE
	.fill 4, CYAN 
	.fill 4, CYAN
	.fill 4, LIGHT_BLUE
	.fill 6, LIGHT_BLUE
	.fill 4, BLUE
	.fill 5, BLUE
	.fill 4, BLUE
	.fill 6, LIGHT_BLUE
	.fill 2, LIGHT_BLUE	
	.fill 4, CYAN
	.fill 4, CYAN
//-----------
*/

	.fill 10, WHITE
	.fill 8, CYAN	//18 
	.fill 9, LIGHT_BLUE //27
	.fill 12, BLUE	//39
	.fill 9, LIGHT_BLUE	//48
	.fill 8, CYAN		//56
/*
	.fill 10+5, WHITE
	.fill 8+4, CYAN	//17 
	.fill 10+5, LIGHT_BLUE //27
	.fill 12+6, BLUE	//42
	.fill 8+4, LIGHT_BLUE	//50
	.fill 8+4, CYAN		//58
*/

scrollCharColorCycleIndex:	
	.byte 0

animsTillMusic:
.print "NR_OF_OF_ANIMS_BEFORE_MUSIC"+ NR_OF_OF_ANIMS_BEFORE_MUSIC
	.byte NR_OF_OF_ANIMS_BEFORE_MUSIC

furbyIntroAnimNote:
	.byte 0,0,0

furbyActive:
	.fill 48,0
animType:	
	.byte 0,0,0	
animCount:		
	.byte 0,0,0		
currentFrame:
	.byte 0,2,2
baseFrame:
	.byte 0,2,2
gotoNextState:	
	.byte 0	
state:	
	.byte STATE_INTRO//STATE_MUSIC//	
activeFurby:
.var af = random()*16
	.byte af
	.byte af+14
	.byte af+30 
	//.byte $80,$80,$80
	
// note is equal to furby index minus a constant	
currentNote:	
	.byte 0,9,18
activeRandomFurby:		
	.byte 0		
activeRandomFurbyNewFrame:		
	.byte 0		
curChar:
	.byte SCROLL_TEXT_OFFSET
		
.align 256
spriteCharStart:
.memblock "charset"
.import binary "charset3_trimmed.bin"

 

.memblock "soundTables1" 
soundTables1() 

 
.import source "sprites.asm"
.align 64
.memblock " 8 scroll sprites"
scrollSpriteMemStart:
.fill 8*64, 0
*=SPRITE_MEM_START
.memblock "sprites" 
readSprites()
scrollSprites:
.memblock "soundroutines" 
.import source "new_soundroutine.asm" 
.memblock "randomChangeFurby"
getRandom:	
	getRandomMacro()	
randomChangeFurby:	
	randomChangeFurbyMacro()
animateRandom:	
	animateRandomMacro()	
.memblock "animator"
.import source "animator.asm"
.memblock "scrollText"
scrollText:	
scrollTextM()
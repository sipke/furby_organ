setupSprites:
{
//setup x positions
	ldy #0
	lda #42
!:	sta $d000,y
	clc 
	adc #36
	iny
	iny
	
	cpy #16
	bne !-

	lda #%11000000
	sta $d010 //MSB

	lda #WHITE//eye white
	sta $d025
	lda #YELLOW//legs and mouth
	sta $d026

	lda #255	// MC			
	sta $d01c 			
	sta $D015		//enable
	sta $d01b	//all sprites behind chars
}		
		rts

/* 
	=================================  COPY CHARS of current frame to apropriate furby =============================  
	A: sprite frame
	X: sprite nr.
*/ 
copySpriteChars: 
	// get address of source data from table which is a table of word data LSB MSB format, so index *2 to  get right byte
	//.eval brkFile.writeln("break exec " + toHexString(*) + " if X==$0 ")

//debug illegal values
	cmp #14
	bcc !+
	break()
	nop
!:	cpx #48
	bcc !+
	break()
	nop
!:
	
 	asl
 	tay
	lda charMapStart,y
	sta ZP_TMP0
	lda charMapStart + 1,y
	sta ZP_TMP1
	// get target address from table which is a table of word data LSB MSB format, so index *2 to  get right byte
	txa
	asl
	tax
	lda charStart,x
	sta sm +1 
	lda charStart + 1,x
	sta sm +2

	ldy #0
!:	lda charOffset,y	// using ofssets from start cell (0,1,2,40,41,42,80,81,82), which also come from table 
	tax
	lda (ZP_TMP0),y
// ECM: add background color	
//	cpy #6
//	bcc sm
//	clc
//	adc #64
sm: sta $ffff,x	
	iny	
	cpy #9	
	bne !-	
		
	rts
//=======================================================================================================================
.macro readSprites(){
	.var file = LoadBinary("sprites.bin")
	.var l = file.getSize()
	.for (var i=0; i<13; i++) {
		.for(var j=0; j<64; j++) {
			.byte file.get(i*128 + j)
		}
	}
}


//============================= CREATE SPRITE DATA, (optimised) CHAR DATA AND CHAR MAPS FROM SPRITE PAD BIN FILE ========= 	
{
	.var chars = List()
	.var maps = List()
	.var sprites= List()
	.var char= List()
	.var map= List()
	.var count =0
	.var exists = false 
	.var source
	.var target 
	
	.const UPPER_BOUNDARY = 2
	.const LOWER_BOUNDARY = UPPER_BOUNDARY + 64
	
	.var file = LoadBinary("sprites.bin")
	.var l = file.getSize()
	
	.var charIndex = 0;
	
	//fill all lists with values
	.var index = 0 
	
	//start with an emty char to fill the screen
	.eval char =List().add(0,0,0,0,0,0,0,0)
	.eval chars.add(char)


// TODO ------------------------------- adjust to right number of sprites!
	.for (var a=0; a<11; a++) {	
//-------------------------------------------		

	//.for (var a=0; a<floor(file.getSize()/128); a++) { 
		//------------ read 64 spritebytes --------------
		.for(var j=0; j<64; j++) {
			.eval sprites.add(file.get(index + j))
		}
		
		.eval index +=64
		
		//  ------------ convert next 64 bytes to 9 (3x3) chars ----------------
		.for(var i=0; i<9; i++) {
			.eval char =List()
			.for (var j = 0; j<8; j++) {
				.eval source = index + (j-1)*3 + mod(i,3) + (24* floor(i/3))
				//.print "source: "+ source
				// if source address is within sprite range read from file
				.if (source >= index && source <index + 63) { 
					.eval char.add(file.get(source))
					//.print "source-index=" + (source-index) + " adding val " + file.get(source) 
				} else {
				//otherwise fill with zeroes
					//.print "source-index=" + (source-index) + " adding 0"
					.eval char.add(0)
				}
				//.print j*3 + mod(i,3) + (24* floor(i/3))
			}
			
			.eval exists = false
			.eval charIndex = chars.size()
			.for(var k=0; k<chars.size();k++){
				.eval count =0
				.for(var m =0; m<8; m++){
					//.print "comparing char " + k + " val " + chars.get(k).get(m) + " to " + char.get(m) 
					.if(chars.get(k).get(m)==char.get(m)) {
						.eval count ++
					}
					.if(count ==8) {		
						.eval exists = true		
						.eval charIndex = k		
					}		
				}
			}
			.eval map.add(charIndex)
			.if (! exists) {
				.eval chars.add(char)
				//.print "new so add"
			}else {
				//.print "already exists don't add" 
			}
			
		}
		.eval index +=64

	}
/*	
@spriteData:	
@spriteDefs:
	.for(var i=0; i<sprites.size(); i++){
		.byte sprites.get(i)
	}
*/
@charMaps:
	//.print "map size"+ map.size()
	.for(var i=0; i<map.size(); i++){
		.byte map.get(i)
		//.print map.get(i)
	}
@charMapStart:	
	.for(var i=0; i<64;i++) { 
		.byte <charMaps+ i*9, >charMaps+ i*9
	}  	
@charOffset:	
	.byte 0,1,2,40,41,42,80,81,82	
@charStart:
	.for(var i=3; i>=0;i--) {
		.word FIRST_CHAR_POS + i*240 + 120 
		.word FIRST_CHAR_POS + i*240       +2
		.word FIRST_CHAR_POS + i*240 + 120 +4
		.word FIRST_CHAR_POS + i*240       +6
		.word FIRST_CHAR_POS + i*240 + 120 +8
		.word FIRST_CHAR_POS + i*240 + 120 +12
		.word FIRST_CHAR_POS + i*240       +14
		.word FIRST_CHAR_POS + i*240 + 120 +16
		.word FIRST_CHAR_POS + i*240       +18
		.word FIRST_CHAR_POS + i*240 + 120 +20
		.word FIRST_CHAR_POS + i*240       +22
		.word FIRST_CHAR_POS + i*240 + 120 +24
	}

	*=$2000	"char defs"
@spriteChars:	
	.for(var i=0; i<chars.size(); i++){
		.for(var j=0; j<8;j++){
			.byte chars.get(i).get(j)
			//.print chars.get(i).get(j)
		}
	}

//============================================== LOGO =====================================
.var logo_start_char = chars.size()
.const LOGO_CHAR_START_0 = $0400 + 33 + 240
.const LOGO_COL_START_0 = $D800 + 33 + 240

.const LOGO_CHAR_START_1 = LOGO_CHAR_START_0 + 238
.const LOGO_COL_START_1 = LOGO_COL_START_0 + 238
.var logofile = LoadBinary("logo.bin")
	.fill 4*8*8, logofile.uget(i)
@drawLogo:	
	ldx #0		
!:	lda charOffsets,x		
	tay		
	lda logoIndexes,x		
	sta LOGO_CHAR_START_0,y
	lda #WHITE
	sta LOGO_COL_START_0,y
	inx
	cpx #12
	bne !- 		

!:	lda charOffsets,x		
	tay		
	lda logoIndexes,x		
	sta LOGO_CHAR_START_1,y
	lda #WHITE
	sta LOGO_COL_START_1,y
	inx
	cpx #32
	bne !- 		
	rts		
//logo is made up of 4x4 blocks		
//goerp chars
//.var charIndexes = List().add(16,17,18,19, 0,1,2,3, 4,5,6,7, 8,9,10,11, 12,13,14,15)
.var charIndexes = List().add(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19)
//tagline
.eval charIndexes.add(20,21,22,23,24,25,26,27,28,29,30,31)	
logoIndexes:	
	.fill 32, charIndexes.get(i) + logo_start_char 	
charOffsets:
	.byte 0,1,40,41		
	.byte 80,81,120,121		
	.byte 160,161,200,201

	.byte 2,3,42,43
	.byte 82,83,122,123
	.byte 160,161,200,201
	.byte 162,163,202,203
	.byte 164,165,204,205
}	
.align 64
orbitalSprite:
	.byte 192+32,0,0
	.byte 192+32,0,0
	.byte 192+32,0,0
	.fill 55,0
	


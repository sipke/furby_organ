//* = $6000 "Sid wizard import"
.const SWM_TEMPLATE = "C64FILE, Header = $0000, Data = $0040"
.const ALPHA = "@ABCDEFGHIJKLMNOPQRSTUVWXYZ." 
.const NOTES = List().add("C","C#", "D","D#","E","F", "F#", "G", "G#", "A", "A#", "B") 
.function char(b) {
	.if(b<$40 || b>$40+27){ .return "."}
	.return ALPHA.charAt(b-$40)
}
//============================== SID WIZARD READER ===================
.const HEADER_LENGTH = $42
.const START_INSTRUMENTS = $ff

	.var file = LoadBinary("./music/furby c 012.swm", SWM_TEMPLATE)
	//.var file = LoadBinary("./music/jinglefurbs5.swm", SWM_TEMPLATE)
	.var ln = file.getSize()
	.var index = HEADER_LENGTH
	.var loadState = 0
	.var sequences = List()
	.var sequence = List()
	.var patterns = List()
	.var pattern = List()
	.var AD = List()
	.var SR = List()
	.var vibSettings = List()
	.var vibDelays = List()
	.var waves = List()
	.var pulseLos = List()
	.var pulseHis = List()
	
	.var b 
	

	.var nrSequences = file.uget($c)
	.var nrPatterns = file.uget($d)
	.var nrInstruments = file.uget($e)
	.var startOfPulse = file.uget($a)
	.var pulseHi
	.var pulseLo
 
.const NR_FRAMES_PER_EVENT = file.uget(ln-2)-$80

	.print "nrSequences" + nrSequences
	.print "nrPattenrs" + nrPatterns
	.print "nrInstruments" + nrInstruments
	.print "nrFramesPerEvent" + (file.uget(ln-2)-$80)//nrFramesPerEvent

	.var instrLength
	.var patternLength
	.var nrPatternEvents
	.var eventLength

// if number instruments = 1 start of instruments

	.var filePoint = ln -2		// ignore last two bytes	
	.var point = filePoint	
	.var name = ""
//======================== READ INSTRUMENTS (for now just use AD and SR values) ==================	
	.for (var i=0; i<nrInstruments; i++) {
		.eval filePoint = filePoint - 9
		.eval name =""
		.for(var j=0; j<8; j++) {
			.eval name = name + char(file.uget(filePoint +j + 1))
		}
		.eval point = filePoint
		.eval instrLength = file.uget(point)
		//.print "instrument "  + i + ": "+ name  + " length " + instrLength
		.eval filePoint = filePoint - instrLength
		//.print "filePoint" + toHexString(filePoint)
		.eval point = filePoint  
 		.eval AD.add(file.uget(point + 3)) //00
 		//.print "AD"+ toHexString(file.uget(point + 3))
 		.eval SR.add(file.uget(point + 4)) //f8
 		//.print "SR"+ toHexString(file.uget(point + 4))
 		
 		.eval vibSettings.add(file.uget(point + 5))
 		.eval vibDelays.add(file.uget(point + 6))
 		
 		.eval waves.add(file.uget(point + 16))

		.eval startOfPulse = file.uget(point + $a)
		.eval pulseHi = file.uget(point + startOfPulse) & $0f
		.eval pulseLo = file.uget(point + startOfPulse + 1)
		.print "pulse value: $" + toHexString(pulseHi) + toHexString(pulseLo)
		.eval pulseLos.add(pulseLo)
		.eval pulseHis.add(pulseHi) 


	}			
// ==================== WRITE INSTRUMEWNTS ===============================
pulseHiTable:
	.byte 0		
	.for(var i=pulseHis.size()-1; i>=0;i--) {	
		.byte pulseHis.get(i)
	}	
	
pulseLoTable:
	.byte 0		
	.for(var i=pulseLos.size()-1; i>=0;i--) {	
		.byte pulseLos.get(i)
	}	

ADTable:
	//extra byte for pause 'instrument' 
	.byte 0		
	.for(var i=AD.size()-1; i>=0;i--) {	
		.byte AD.get(i)
	}	
attackTable:		
	//extra byte for pause 'instrument' 
	.byte 0		
	.for(var i=AD.size()-1; i>=0;i--) {	
		.byte AD.get(i) >>4
	}	
decayTable:		
	//extra byte for pause 'instrument' 
	.byte 0		
	.for(var i=AD.size()-1; i>=0;i--) {	
		.byte AD.get(i) & 15
	}	
SRTable:
	//extra byte for pause 'instrument' 
	.byte 0		
	.for(var i=SR.size()-1; i>=0;i--) {	
		.byte SR.get(i)
	}	
sustainTable:	
	//extra byte for pause 'instrument'	
	.byte 0		
	.for(var i=SR.size()-1; i>=0;i--) {	
		.byte SR.get(i) >> 4
	}	
releaseTable:	
	//extra byte for pause 'instrument'	
	.byte 0		
	.for(var i=SR.size()-1; i>=0;i--) {	
		.byte SR.get(i) & 15
	}	
vibDelayTable:		
	.byte 0		
	.for(var i=vibDelays.size()-1; i>=0;i--) {	
		.print "vibrato delay"+vibDelays.get(i) 	
		.byte vibDelays.get(i)
	}	
vibAmplitudeTable:		
	.byte 0		
	.for(var i=vibSettings.size()-1; i>=0;i--) {	
		.print "vibrato amplitude " + (vibSettings.get(i) & $F0)	
		.byte vibSettings.get(i) & $F0
	}	
vibFreqTable:		
	.byte 0		
	.for(var i=vibSettings.size()-1; i>=0;i--) {	
		.print "vibrato frequency" + (vibSettings.get(i) & 15)	
		.byte vibSettings.get(i) & 15
	}	
	
waveTable:	
	.byte 0		
	.for(var i=waves.size()-1; i>=0;i--) {	
		.print "waveform $" + toHexString(waves.get(i))	
		.byte waves.get(i)
	}	
		
			   
 // ========================== READ PATTERNS (backwards) ========================
// now filepoint is at end of patterns
	.eval filePoint = filePoint-1
	.var count 
	.var unpackedPattern
	.var prev
	.for (var i=0; i<nrPatterns; i++) {
		//.print "filePoint" + toHexString(filePoint)
		
		.eval nrPatternEvents = file.uget(filePoint)
		.eval patternLength  = file.uget(filePoint -1) + 1
		.eval unpackedPattern = List()
		
		//tricky stuff: patternLength is length including packed NOPs
		//work backwards and count nops until real length determined 
		//.print "pattern " + i + " length " + patternLength  + " nr events: " + nrPatternEvents
		
		.eval filePoint = filePoint-2
		.eval point= filePoint 		
		
		.eval count =0	//amount of packed zeroes in pattern 
		.for (var j =0; j<patternLength-2; j++){		
			.eval b = file.uget(point)		
			.eval prev = file.uget(point-1)		
			.if (b >=$70 && b<=	$77 && (prev==0 || (prev>=$70 && prev<=$77))){	
				.for (var k=0; k<b-$70+2; k++) {	
					.eval unpackedPattern.add(0)	
					.eval j =  j + 1	
				}	
				.eval j =  j - 1
			} else {	
				.eval unpackedPattern.add(b)
			}	
			.eval point = point - 1
		}		
		.eval pattern =List()		
		.for (var j=0; j<unpackedPattern.size(); j++) {
			.eval pattern.add(unpackedPattern.get(unpackedPattern.size()-j-1))
			//.print toHexString(unpackedPattern.get(unpackedPattern.size()-j-1))
		}
		.eval patterns.add(pattern)

		.eval filePoint = point
	}

//========================= READ SEQUENCES =========================================
// reading sequences 
	.for (var i = HEADER_LENGTH-2; i<ln; i++) {
		.eval b= file.uget(i)
		// reading patterns
		.if(loadState >3 && loadState !=START_INSTRUMENTS) {
			.if(b==$ff){
				.eval loadState = loadState + 1
				.eval i = i+2
				.eval sequences.add(sequence)
				.eval sequence = List()
				//.print "new state" + state 
			} else {
				// subtract 1 because list is zero indexed, but in Sid Wizard starts with 1
				.eval sequence.add(b - 1)
			}
		}
		// reading tracks
		.if(loadState==0 || loadState==1 ||loadState==2 ) {
			// sequence 0
			.if(b==$ff){
				.eval loadState = loadState + 1
				.eval i = i+2
				.eval sequences.add(sequence)
				.eval sequence = List()
				//.print "new state" + state 
			} else {
				// subtract 1 because list is zero indexed, but in Sid Wizard starts with 1
				.eval sequence.add(b - 1)
			}
		}
	}  
//======================================= GENERATE TABLES ===========================	

.var p
.var s
.var curInstr
.var notes
.var lengths
.var instruments

.var nrEventsInTrack = List()
.for (var i=0; i<sequences.size(); i++){
//.print "track " + i
.eval s = ""
.eval notes = List()
.eval lengths = List()
.eval instruments = List()

//.for (var j=0; j<sequences.get(i).size(); j++){
//	.eval p = nrPatterns - sequences.get(i).get(j) - 1
//	.eval s = s + (16-p) + " "
//}
.print s
.for (var j=0; j<sequences.get(i).size(); j++) {
	.eval p = nrPatterns - sequences.get(i).get(j) - 1
	//.print "pattern " + p
	//.eval s = "" 
	.eval ln=0;
	.eval curInstr = 1
	.for (var k = 0; k <patterns.get(p).size(); k++){
		//.eval s = s + toHexString(patterns.get(p).get(k)) + " "
		.eval b = patterns.get(p).get(k)
		.if(b> $80) {
			.if(ln!=0) {
				.eval lengths.add(ln)
	//			.print "write length " + ln
			}
			.eval curInstr = patterns.get(p).get(k + 1)
//			.print "add instrument " + curInstr
			.eval instruments.add(curInstr)
			.eval notes.add(b -$80 - FURBY_INDEX_TO_NOTE_OFFSET - TRANSPOSE_SID_WIZARD_AMOUNT)
			.eval ln = NR_FRAMES_PER_EVENT
			.eval k=k + 1
		}else{
			.if(b==$7e) {
				.if(ln!=0) {
//					.print "write length " + ln
					.eval lengths.add(ln)
				}
//				.print "add rest "
				.eval instruments.add(0)
				.eval notes.add(0)
				.eval ln = NR_FRAMES_PER_EVENT
			} else {
				.if(b==0) {
					//if pattern starts with no note
					.if(k==0) {
						.eval instruments.add(0)
						.eval notes.add(0)
					}
					.eval ln = ln +NR_FRAMES_PER_EVENT
//					.print "adding length" 
				}else {
					.if(ln!=0) {
//						.print "write length " + ln
						.eval lengths.add(ln)
					}
					//.print "add note "
					.eval instruments.add(curInstr)
					.eval notes.add(b -FURBY_INDEX_TO_NOTE_OFFSET - TRANSPOSE_SID_WIZARD_AMOUNT)
					.eval ln = NR_FRAMES_PER_EVENT
				}
			}
		}
		
	}

	//.print "write length " + ln
	.eval lengths.add(ln)
	//.print s
}
.if(i ==0) {		
.eval instrumentTable0 = *
}		
.if(i ==1) {		
.eval instrumentTable1 =*
}		
.if(i ==2) {		
.eval instrumentTable2 =*
}		
//.eval s = "instrumentTable" + i + ": .byte "
.for(var j=0; j<instruments.size(); j++) {
	//.eval s = s + instruments.get(j) + ", "
	//.assert "instrument >=0<16" ,true, (instruments.get(j)>=0 && instruments.get(j)<16) 
	.byte instruments.get(j)
}
	.byte $ff
//.print s
.if(i ==0) {		
.eval lengthTable0 =*
}		
.if(i ==1) {		
.eval lengthTable1 =*
}		
.if(i ==2) {		
.eval lengthTable2 =*
}		
//.eval s = "lengthTable" + i + ": .byte "
.for(var j=0; j<lengths.size(); j++) {
	//.assert "length >0<256" ,true, (lengths.get(j)>0 && lengths.get(j)<256)
	//.eval s = s + lengths.get(j) + ", "
	.byte lengths.get(j)-1
}

.if(i ==0) {		
.eval noteTable0 = *
}		
.if(i ==1) {		
.eval noteTable1 = *		
}		
.if(i ==2) {		
.eval noteTable2 = *
}		
//.print s
//.eval s = "noteTable" + i + ": .byte "
.for(var j=0; j<notes.size(); j++) {
	//.eval  s = s + notes.get(j) + ", "
	//.assert "note >=0<64 " + notes.get(j) ,true, (notes.get(j)>=0 && notes.get(j)<64)
	.byte notes.get(j)
	.if(instruments.get(j)==0) {
		.print "rest, length:" + lengths.get(j)
	}else {
		.print "furby:"+ notes.get(j) + "(" + NOTES.get(mod(notes.get(j)+FURBY_INDEX_TO_NOTE_OFFSET-13,12)) + (floor((notes.get(j)+FURBY_INDEX_TO_NOTE_OFFSET-13)/12)+ 2) + ")" + " length:" + lengths.get(j) + " instrument:" + instruments.get(j)
	}
}
 
//.print s
	.eval nrEventsInTrack.add(notes.size())
	.print "instr count:" + (instruments.size()) 
	//.assert "sizes equal length note" + notes.size()+" "+lengths.size(), true , notes.size()==lengths.size()
	//.assert "sizes equal instr length"+ instruments.size()+" "+lengths.size(), true , instruments.size()==lengths.size()
}

	
nrEvents:
	.byte nrEventsInTrack.get(0), nrEventsInTrack.get(1), nrEventsInTrack.get(2)
	.print "nr events: " + (nrEventsInTrack.get(0)) + "," + (nrEventsInTrack.get(1)) + "," + (nrEventsInTrack.get(2))

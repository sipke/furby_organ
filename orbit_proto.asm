.var brkFile = createFile("debug.txt")
.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}



.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		


.pc = $0810 "setup" 
	
	lda #105+128
	sta $0400+13+400
	lda #95+128
	sta $0400+13+401
	lda #95
	sta $0400+13+440
	lda #105
	sta $0400+13+441
	
	lda #BLACK
	sta $d02e
	
	ldx #0
	lda #0
!:	sta 832,x	
	inx	
	cpx #64	
	bne !-	
	lda #192	
	sta 832	
	sta 835	
		
	lda #13	
	sta 2047	

	lda #128
	sta $d000
	sta $d001
	
	lda #128
	sta $d015
	
	             lda #<int
             sta $0314
             lda #>int
             sta $0315

             lda #230
             sta $d012
             lda #$1b
             sta $d011

             lda #$01
             sta $d01a        // enable raster IRQ  

	
	rts

int:	
	lsr $d019	
	bcs doInt	
	jmp $ea31
	
doInt:		
	inc $d020
calcvx()
inc $d020
calcvy()
inc $d020
calcPos()
	dec $d020
	dec $d020
	dec $d020
	jmp $ea81

.import source "orbit.asm"
	

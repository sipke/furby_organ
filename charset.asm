// code from https://csdb.dk/release/?id=163155 
// char set by Zirias
lines:		.byte	$ed
pages:		.byte	$8
.const buf1r		= $55
.const buf1		= $56
.const buf1t		= $5c
.const buf2		= $5d

createCharset:
{
	lda $1
	and #%11111011
	sta $1
	
		ldy	#$0
		sty	$fe
		sty	buf1r
		sty	buf1t
loop:		ldx	#$6
.var rdpage		= *+2
rdloop:		lda	$d000,y
		iny
		ror	lines
		bcc	rdloop
		and	#$7e
		sta	buf1r,x
		dex
		bne	rdloop
		ror	lines
		sty	$fd
		stx	buf2+6
		stx	buf2+7
		ldx	#$5
olloop:		lda	buf1,x
		sta	buf2,x
		ora	buf2+2,x
		sta	buf2+2,x
		lda	buf1,x
		asl	
		ora	buf2+1,x
		sta	buf2+1,x
		lda	buf1,x
		lsr	
		ora	buf2+1,x
		sta	buf2+1,x
		dex
		bpl	olloop
		ldy	$fe
		ldx	#$7
wrloop:		lda	buf2,x
		eor	buf1r,x
		eor #$ff
.var wrpage		= *+2
		sta	CHAR_SET_START,y
		iny
		dex
		bpl	wrloop
		sty	$fe
		ldy	$fd
		bne	loop
		inc	wrpage
		inc	rdpage
		dec	pages
		bne	loop
	//enable IO	
	lda $1
	ora #%00000100
	sta $1
	//write to IO where charset resides
		lda $d018
		and #%11110001
		ora #%00001000

		sta	$d018

		rts
}

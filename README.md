# Furby organ
64 sprites of furbies each responsible for a single (or 2 notes)
notes and animation triggered by a tracker player
intended as an intro

## stretch goal
add a scroller outside the bottom border 

maybe add lame flashing border as beat accent?

music should be made in a way so there is enough variation in furbies animating
to make it visually pleasing as well (tricky!)

so probably walking bass 

two note chords, like old C64 arcade classics?, upbeat melody

maybe some  ripoff of old song? https://www.youtube.com/watch?v=68AnYzIceb0
Frantic freddie


maybe arpeggios

fast vibrato because furbies talk like that
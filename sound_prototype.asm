.var brkFile = createFile("debug.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 "setup" 

	sei
             lda #<int
             sta $0314
             lda #>int
             sta $0315

             lda #80
             sta $d012
             //lda $d011		// bit 8 =0
             //and #254
             lda #$1b
             sta $d011

             lda #$01
             sta $d01a        // enable raster IRQ  

	cli			
	jmp *			
				
int:				
	lda $d019				
	and #%0000001				
	bne !+				
	jmp $ea31				
					
!:					
	inc $d020				
					
	ldx #0				
!:	lda $ffff,x				
	inx				
	bne !-				
					
	dec $d020				
	lsr $d019				
	jmp $ea81				
					

//							STABILIZE RASTER AND DRAW WITH BACKGROUND COLOR
.macro stabilizeRaster(){
	ldx #SCROLL_LINE -4//$d012
	inx
!:	cpx $d012
	bne !- 

			lda #<int2														//				2
             sta $fffe														// 				6
             lda #>int2													//					8								
             sta $ffff        					//											12

//set new raster interrupt, don't forget >256 
			lda $d011
			ora #%10000000
			sta $d011

  			 inc $d012			// raster interrupt at next line					timing: 	18										
             lda #$01														//				20
             sta $d019			// acknowledge irq											24
             tsx 				// not normal ending of irq, so stack pointer 				26
             					// will be broken, so save stack pointer 					
             cli									//										28
             
             nop									//										
             nop
             nop				
             nop
             nop
             nop
             nop
             nop
             nop				
             nop
             nop
             nop
             nop
             nop								// 									8x2= 	44
		//                                             interrupt handling:		20-27 ->64-71 cycles 
             nop
             nop
             nop
             nop
    
    //--------------------------------------------------
    	// by now irq has been triggered while performing nop's 
int2:     	txs					// repair stack pointer 
    		ldx #$08
    		dex
    		bne *-1
    		bit $00				// a three cycles 'neutral' instruction
    		lda $d012			// see if  
    		cmp $d012			// raster changes at this point
    		beq *+2				// if not-> do branch, which takes 1 cycle more than not taking branch
              
    //--------------------------------------------------
}

.macro delay(nrCycles) {
	.if(nrCycles==1) {
		.error("can't delay one cycle")
	}else {
		.if(mod(nrCycles,2)==0){
			.fill nrCycles/2, NOP
		}else {
			bit $01		
			.fill (nrCycles-3)/2, NOP
		}
	}
}
.macro getRandomMacro() {
	lda randomSeed
    beq doEor
    clc
    asl
    beq noEor    //if the input was $80, skip the EOR
    bcc noEor
doEor:      
    eor #$1d//B8//#$1d
noEor:      
      sta randomSeed
	rts
}
.macro randomChangeFurbyMacro() {
	lda randomSeed
	ldx state
	cpx #STATE_MUSIC
	beq chanceIncrease
	lda randomSeed
	bmi end
	jsr getRandom
	bmi end
	jsr getRandom
	bmi end
chanceIncrease:	
	and #%00111111	//up to 63
	cmp #47	//if larger than 47, just shift right, means some numbers have a bigger change of appearing but nevermind 
	bcc !+
	lsr
!:	tax 
	lda furbyActive,x
	beq !+
	lda #$80	//negative number to indicate there's no furb to change
	sta activeRandomFurby
	rts
!:	stx activeRandomFurby
	lda FURB_FRAME_ZP_START,x
	sec
	sbc #(SPRITE_MEM_START/64)
	cmp #SLEEP_FRAME_0
	bne checkSleepy
	ldy state
	cpy #STATE_MUSIC
	bne !+
	lda #$80	//negative number to indicate there's no furb to change
	sta activeRandomFurby
	rts
!:	lda #SLEEPY_FRAME	
	jmp store
checkSleepy:	
	cmp #SLEEPY_FRAME
	bne default
	lda #SLEEP_FRAME_0
	jmp store
default:	
	lda #SLEEPY_FRAME
store:	
	cmp #13	
	bcc !+	
	break()	
	nop	
!:	sta activeRandomFurbyNewFrame  
end:	
	rts
}
.macro animateRandomMacro() {
	ldx activeRandomFurby
	bmi dontAnimate
	lda activeRandomFurbyNewFrame
	
	jsr copySpriteChars
 	
	ldx activeRandomFurby
	lda activeRandomFurbyNewFrame	
	clc	
	adc	#(SPRITE_MEM_START/64)
	sta FURB_FRAME_ZP_START,x	
	lda #$80	
	sta activeRandomFurby	
dontAnimate:		
	rts
}
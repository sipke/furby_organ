.macro scrollTextM() {
	.encoding "screencode_upper"
	//              1         2                   4                   6                  8          10                 12                  14                  16        17        18        19         20       21               
	//     123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
	.text                                                                                                                                                                                                      "  FURBIES AWAKEN! . . . . . . . . . NOW SING!             "                                  
	.text "THIS DEMO WAS INSPIRED BY THE PHYSICAL FURBY ORGAN BY LOOK MUM NO COMPUTER. GRAPHICS MUSIC AND CODE BY GOERP. FONT BY UNKNOWN. MANY THANKS TO THE CREATORS OF SID WIZARD SPRITE PAD CHAR PAD AND C64 DEBUGGER. HAPPY NEW YEAR TO THE WHOLE C64 SCENE!          "
}	

.macro scrollMacro(line) {	
.if(PERF==true) { inc $d020 }
	lda doLargeScroll	
	bne !+
	.if (line==0) {		jsr getRandom	}
	.if (line==1) {		jsr randomChangeFurby	}
	.if (line==2) {		jsr animateRandom	}
	

	jmp end	
!:	
	
	.for (var i=0; i<8;i++){
		//move bytes from right to left, starting left
		lda scrollSpriteMemStart + line*64 + 1 + i*3
		sta scrollSpriteMemStart + line*64 + 0 + i*3
		lda scrollSpriteMemStart + line*64 + 2 + i*3
		sta scrollSpriteMemStart + line*64 + 1 + i*3

		//rightmost byte gets value from leftmost byte of sprite to the right
		//unless it is the rightmost sprite
		.if(line !=7){		
			lda scrollSpriteMemStart + line*64 + 64 + i*3
			sta scrollSpriteMemStart + line*64 + 2 + i*3
		} 
	}
	//if rightmost sprite
	.if (line==7) {

		ldy curChar
readchar:		
		lax scrollText-SCROLL_TEXT_OFFSET,y
		//divide by 32 to get page
		lsr
		lsr
		lsr
		lsr
		lsr
		clc
		adc #>spriteCharStart
		sta sm1 + 2
		txa	//times 8 to get char definition
		asl
		asl
		asl
		sta sm1 +1
		ldx #0
		ldy #0
sm1:	lda $ffff,x			
		sta scrollSpriteMemStart + 7*64 + 2,y 			
		inx				
		iny				
		iny				
		iny				
		cpx #8				
		bne sm1				
		inc curChar						
					
 						
//if end of page and we're not yet increased it								
//start music and goto next page of text								
		bne notNextStage						
		lda readchar + 2						
		cmp #>(scrollText-SCROLL_TEXT_OFFSET)						
		bne notNextStage						
								
	//this is done by setting an indicator so next frame the state will change. 
	//This means all running animations can be set to neutral this frame
	lda #1
	sta gotoNextState	
	lda #SLEEP_FRAME_0	
	sta currentFrame + 0	
	sta currentFrame + 1	
	sta currentFrame + 2	
//make sure all filters are off		
	lda #0		
	sta $d417		
		
// sync color cycle with start music	
	lda #0	
	sta scrollCharColorCycleIndex	
								
//next page of text
		inc readchar + 2						
notNextStage:								
	//now indicate dolargescroll is done	
		lda #0
		sta doLargeScroll
	}
end:	
.if(PERF==true) {dec $d020}	
}
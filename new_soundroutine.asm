.var startimport = *
.import source "sidwizard_import.asm"
.print "sidwizard size = " + (*-startimport)

.const EXPTRESHOLD = 107

initSound:
	ldx #0
	lda #0
!:	sta SID,x	
	inx	
	cpx #24	
	bne !-	
//set volume and filter, only one filter applies throught, so can be set once		
	//low pass filter, max volume
	lda #$0f
	sta $d418
	//resonance filter at C
	//lda #$c0
	//sta $d417

	//lda #$30
	//sta $d416		//filter high freq cutoff

	lda #<instrumentTable0	
	sta ZP_INSTR_0	
	lda #(>instrumentTable0) + 1 	
	sta ZP_INSTR_0 + 1	
	lda #<instrumentTable1	
	sta ZP_INSTR_1  	
	lda #(>instrumentTable1) + 1	
	sta ZP_INSTR_1 + 1	
	lda #<instrumentTable2	
	sta ZP_INSTR_2	
	lda #(>instrumentTable2) +1 
	sta ZP_INSTR_2 + 1	

	lda #<lengthTable0	
	sta ZP_LENGTH_0	
	lda #(>lengthTable0)	+1
	sta ZP_LENGTH_0 + 1	
	lda #<lengthTable1
	sta ZP_LENGTH_1  	
	lda #(>lengthTable1)	+ 1
	sta ZP_LENGTH_1 + 1	
	lda #<lengthTable2	
	sta ZP_LENGTH_2	
	lda #(>lengthTable2)	+ 1
	sta ZP_LENGTH_2 + 1	

	lda #<noteTable0	
	sta ZP_NOTE_0	
	lda #(>noteTable0) + 1	
	sta ZP_NOTE_0 + 1	
	lda #<noteTable1	
	sta ZP_NOTE_1  	
	lda #(>noteTable1) + 1	
	sta ZP_NOTE_1 + 1	
	lda #<noteTable2	
	sta ZP_NOTE_2	
	lda #(>noteTable2) + 1	
	sta ZP_NOTE_2 + 1	

	lda #nrEventsInTrack.get(0) -1
	sta placeInSong + 0
	lda #nrEventsInTrack.get(1) -1
	sta placeInSong + 1
	lda #nrEventsInTrack.get(2) -1
	sta placeInSong + 2

	rts	

// incapsulated so I can move it around easily
.macro soundTables1() {

@currentSoundEventQueue0:
	.byte 0,0,0
@currentSoundEventQueue1:
	.byte 0,0,0
@currentSoundEventQueue2:
	.byte 0,0,0
@currentSoundStep:
	.byte 0,0,0

@restartMusicAtNote:
	.byte 4,4,4

@instrument:
	.byte 0,0,0

@attackDecay:
	.byte 0,0,0
@sustain:
	.byte 0,0,0
@release:
	.byte 0,0,0

@freqLo:
	.byte 0,0,0
@freqHi:
	.byte 0,0,0

@vibDelay:		
	.byte 0,0,0		
@vibAmplitude:		
	.byte 0,0,0		
@vibFreq:		
	.byte 0,0,0

@vibratoValueLo:
	.byte 0
@vibratoValueHi:
	.byte 0

@vibFreqLo:
	.byte 0,0,0
@vibFreqHi:
	.byte 0,0,0
// from sid wizard: CURRENT DISCRETE NOTE-PITCH (ISN'T AFFECTED BY EFFECTS)	
@DPITCH:	
	.byte 0,0,0	
		
@vibWidth: 	
	.byte 0,0,0	
@vibCount:	
	.byte 0,0,0	
@doVib:	
	.byte 0,0,0	
@vibUp:
	.byte 0,0,0

@voiceTmp:
	.byte 0
@lengthTmp:	
	.byte 0	
@freqLoTmp:
	.byte 0
@freqHiTmp:
	.byte 0
@vibFreqLoTmp:
	.byte 0
@vibFreqHiTmp:
	.byte 0

@index9:
	.byte 0
@index7:
	.byte 0
@index1:
	.byte 0
@index3:	
	.byte 0	
@noteTmp:
	.byte 0
@attackTmp:
	.byte 0
@decayTmp:
	.byte 0
@sustainTmp:
	.byte 0
@pulseLoTmp:	
	.byte 0	
@pulseHiTmp:	
	.byte 0	
@sustainDiv4:	
	.byte 0 	
@attackLengthTmp:
	.byte 0
@decayLengthTmp:
	.byte 0
@decayOffsetIndex:
	.byte 0
@decayValue:
	.byte 0
@attackDecayTmp:	
	.byte 0	
@sustainReleaseTmp:	
	.byte 0	
@instrTmp:	.byte 0

// 1 frame = 1/50 or 1/60 sec = 20 or ~16 ms	
			//	0		1		2		3		4		5		6		7		8		9		A		B		C		D		E		F	
			//	2,  	8,	   16, 	   24,	   38,	   56,	   68,	   80,	  100,	  250,	  500,	  800,   1000,   3000,   5000,   8000
.var als = List().add(2,  	8,	   16, 	   24,	   38,	   56,	   68,	   80,	  100,	  250,	  500,	  800,   1000,   3000,   5000,   8000)			
@attackFrameCount:			
	//.fill 16, ceil(als.get(i)/16)		
	.byte 1,1,1,2,3,4,5,5,7,16,32,50,63,188,255,255		//last numbers are actually 313,500 but those won't work
			// decay, release

.var dls = List().add(6,      24,     48,     72,    114,    168,    204,    240,    300,    750,   1500,   2400,   3000,   9000,  15000,  24000) 			
@decayFrameCount: 
@releaseFrameCount: 
	//.fill 16, ceil(dls.get(i)/16)
	.byte 1,2,3,5,8,11,13,15,19,47,94,150,188,255,255,255//last numbers are actually 563,938,1500 but those won't work 		
 			// 0        1       3       4      6        9      10      12      15      38      75     120     150     450     750    1200
@attackLengthPerFrame:
	.byte 0,0,0,1
	.byte 0,0,0,1
	.byte 0,0,0,1
	.byte 0,1,0,1
	.byte 1,1,0,1
	.byte 1,1,1,1
	.byte 1,1,1,2
	.byte 1,1,1,2
	.byte 2,2,1,2
	.byte 4,4,4,4
	.byte 8,8,8,8
	.byte 12,13,12,13
	.byte 16,15,16,16
	.byte 44,45,44,45
	.byte 64,64,64,64//79,78,78,78
	.byte 64,64,64,64//125,125,125,125
@decayLengthPerFrame:
	.byte 0,0,0,1
	.byte 0,1,0,1
	.byte 0,1,1,1
	.byte 1,1,1,2
	.byte 2,2,2,2
	.byte 2,3,3,3
	.byte 3,3,3,4
	.byte 4,4,3,4
	.byte 4,5,5,5
	.byte 12,11,12,12
	.byte 23,24,23,24
	.byte 38,37,37,38
	//------------ any lengths adding up to 256+ will lead to problems, because total note length is max 256  
	//----- so really shouldn't use any decay >9   	
	.byte 44,45,44,45
	.byte 64,64,64,64//141,141,140,141
	.byte 64,64,64,64//234,235,234,235  // 
	.byte 64,64,64,64//255,255,255,255 // actually should be 375,375,375,375

}

// TODO: now limited to 255 notes, is not much	
//                                          ------------ song ----------------------------

placeInSong:
	.byte $ff,$ff,$ff
restartAt:
	.byte 0,0,0	
//----------------------------------------------------------------------------------------------	
	
@doVoice0:
	lda state
	cmp #STATE_MUSIC
	bne !+
	jmp doMusic0
//!:	premusic(0)
!:	rts
doMusic0:	
	playStep(0)
@doVoice1:
	lda state
	cmp #STATE_MUSIC
	bne !+
	jmp doMusic1
//!:	premusic(1)
!:	rts
doMusic1:	
	playStep(1)
@doVoice2:
	lda state
	cmp #STATE_MUSIC
	bne !+ 
	jmp doMusic2
//!:	premusic(2)
!:	rts
doMusic2:	
	playStep(2)

//-------------------------- PRE MUSIC SOUNDS-----------------------------------

.macro premusic(index) {
	lda activeFurby + index
	bpl !+
	jmp end0
!:	lda animType + index
	cmp #1
	bne yawn
// -------not ywan
//.if(index==1){break()}
/*
	lda animCount + index
	cmp #48		// at end?
	bcc !+
	lda #$10
	sta $d404 + index*7
	rts

!:	

	and #$02	//every 4th frame start release
	beq !+
	lda #$10
	sta $d404 + index*7
	rts

!: lda animCount + index	
	and #$08	// every 8th frame start attack
	beq end1	
	lda #$11
	sta $d404 + index*7
	ldx furbyIntroAnimNote + index
	dex
	stx furbyIntroAnimNote + index
	lda FreqTablePalLo,x
	sta $d400+ index*7
	//sta freqLo + index
	lda FreqTablePalHi,x 
	sta $d401+index*7
end1:	
*/	
	rts
yawn:		// yAWN	
	lda animCount + index
	cmp #48
	bcc !+
	lda #$10
	sta $D404 + index*7
	rts
!:	cmp #24
	bcc add
subtract:
	lda freqLo + index
	sec
	sbc #64
	sta freqLo + index
	sta $D400 +index*7
	lda freqHi + index
	sbc #0
	sta freqHi +index
	sta $D401 +index*7
	rts	
add:	
	lda freqLo +index
	clc
	adc #64
	sta freqLo +index
	sta $D400 +index*7
	lda freqHi +index
	adc #0
	sta freqHi +index
	sta $D401 +index*7
end0:	
	rts
}
	
//--------------------------------------- main music routine
.macro playStep(index) {	
	lda currentSoundEventQueue0 + 3*index	//length left in step
	beq doNextStep
	cmp #2
	bne !+	
hardRestart:
//activate release and hard restart 	
	lda #$0f	
	sta SID + 5 + index*7	
	lda #$00	
	sta SID + 6 + index*7	
	ldx instrument + index	
	lda waveTable,x	
	and #%11111110	
	sta SID + 4 +index*7
//TODO; really do this? interferes with animation	
	lda #SLEEP_FRAME_0	
	sta currentFrame + index	
	sta baseFrame + index	
	lda currentSoundEventQueue0 + 3*index	//length left in step
!:	sec	
	sbc #1	
	sta currentSoundEventQueue0 + 3*index
	rts
doNextStep:	
//note ended, set furby to non-active and get new note		
	ldx activeFurby + index
	bpl isActive
	jmp newNote 	//there is no active furby skip to new note
isActive:	
	lda #0
	sta  furbyActive,x
	//dec furbyActive,x
	lda #$80
	sta activeFurby + index
newNote:
	//IF ZP_INSTR_0 + 1 + 2*index == >instrumentTable0 comnpare with zero, if so, increase ZP_INSTR_0 + 1 + 2*index (also length and note)
	//											else  compare with nr events if so decrease ZP_INSTR_0 + 1+ 2*index (also length and note) 
	//																				and x = 0

	ldy placeInSong + index
	
	lda ZP_INSTR_0 +1 + 2*index
	.if (index==0) cmp #>instrumentTable0
	.if (index==1) cmp #>instrumentTable1
	.if (index==2) cmp #>instrumentTable2  
	bne compareEnd
	iny
	bne doneEval
	inc ZP_INSTR_0 +1 + 2*index
	inc ZP_NOTE_0 +1 + 2*index
	inc ZP_LENGTH_0 +1 + 2*index
	jmp doneEval 	
compareEnd:	
	iny
	cpy nrEvents + index
	bne doneEval
	.print "RESTART_MUSIC_AT_NOTE"  + RESTART_MUSIC_AT_NOTE.get(index)
// nasty piece of code so only first time restart at 0	
	ldy restartAt + index
	lda #RESTART_MUSIC_AT_NOTE.get(index)
	sta restartAt + index
	dec ZP_INSTR_0 +1 + 2*index
	dec ZP_NOTE_0 +1 + 2*index
	dec ZP_LENGTH_0 +1 + 2*index

doneEval:	sty placeInSong + index

	lda #index*7
	sta index7
	lda #index
	sta index1
	lda #index*3
	sta index3
	lda #index*9
	sta index9
	
	.if(index ==0) {
		lda (ZP_LENGTH_0),y
		sta lengthTmp
		sta currentSoundEventQueue0 + index*3 
		lda (ZP_INSTR_0),y
		sta instrTmp
		sta instrument + index
		lda (ZP_NOTE_0),y
	}
	.if(index ==1) {
		lda (ZP_LENGTH_1),y
		sta lengthTmp
		sta currentSoundEventQueue0 + index*3 
		lda (ZP_INSTR_1),y
		sta instrTmp
		sta instrument + index
		lda (ZP_NOTE_1),y
	}
	.if(index ==2) {
		lda (ZP_LENGTH_2),y
		sta lengthTmp
		sta currentSoundEventQueue0 + index*3 
		lda (ZP_INSTR_2),y
		sta instrTmp
		sta instrument + index
		lda (ZP_NOTE_2),y
	}
	sta activeFurby + index
	tay
	tax
	lda #1
	sta  furbyActive,x
	tya
	clc	
	adc #FURBY_INDEX_TO_NOTE_OFFSET	
	sta currentNote + index	
	sta DPITCH + index	
	tax	
	lda FreqTablePalHi,x
	sta SID + 1 + index*7
	sta freqHi + index
	lda FreqTablePalLo,x
	sta SID  + index*7 	
	sta freqLo + index

		ldy instrTmp
		lda pulseLoTable,y
		sta $D403 + index*7
		lda pulseHiTable,y
		sta $D402 + index*7
		lda attackTable,y
		sta attackTmp
		lda decayTable,y
		sta decayTmp
		lda ADTable,y
		sta attackDecayTmp
		sta $D405 + index*7
		lda SRTable,y
		sta sustainReleaseTmp
		sta $D406 + index*7
		lda sustainTable,y
		sta sustainTmp

	jmp doGeneric
}
doGeneric:
{
		cpy #0
		bne notSilent
//instrument = 0: silent
		ldy index7
		ldx index1
		lda #$10
		sta $D404,y
		// set vibratodelay to max // not necessary but easier for debugging
		lda #$ff
		sta vibDelay,x
		//set animation to end frame, neutral		
		lda #8
		sta animationQueuePosition,x
		//with length of MAX_LENGTH, the sound length of note is leading, and is $ff or shorter
		ldy index9
		lda #$ff
		sta frameLengths+8,y
		
		lda #$80	//minus: no active furby for that voice
		sta activeFurby,x
		jmp end
notSilent:		
//---------------------- vibrato setup copied from sidwizard ----------------
.var EXPTBASE = EXPTABH
//.var EXPTBASE = FreqTablePalHi-1//FREQTBH-1
.var EXPTRESHOLD = ENDFREQTBH-EXPTBASE

//		y= instrTmp

SETVIB0: ldx index1  //READ VIBRATO-DELAY/INCREMENT AMOUNT FROM INSTRUMENT-DATA
		lda vibDelayTable,y
        sta vibDelay,x  //MAX.$7F, COUNTS BACK TILL $FF
SETVIB1: 
		lda vibFreqTable,y
SETVIBR:              //SET AMPLITUDE AND RATE OF VIBRATO. INPUT: ACCU (HI-NIBBLE IS AMPLITUDE)
        asl             //ASL, BIT 1 IS ALWAYS 0, SO IT CAN BE CLIPPED INTO 2 EQUAL TIMEFRAMES FOR SLIDE UP/DOWN
        sta vibFreq,x
        lsr             //HALF-TIMERSTART FOR DOWN-ORIENTED VIBRATO (LIKE GUITAR TREMOLO-ARM)
        lsr             //FOR QUARTER TIMERSTART - NORMAL VIBRATO
setVcnt: 
		sta vibCount,x
        lda vibAmplitudeTable,y
SETVAMP: //SET AMPLITUDE
        lsr             //0..127
SETFMOD:                 //SET FREQUENCY-MODIFIER VALUES FOR SLIDE/PORTAMENTO, OR RESET THEM IF NO SLIDE/VIBRATO
        beq wrFmodL     //IF 0, NO CALCULATION OF PITCH
        lsr             //vibrato-0..64, slide-0..127
        adc DPITCH,x    //vibrato-0..160, slide-0..222 , CALCULATE PITCH-DEPENDENT AMPLITUDE-COMPENSATION HERE!!!!
LOOKUPA: tay             //0..LOOK UP AMPLITUDE VALUE FROM FREQUENCY/EXPONENT-TABLES
CHKTEND: cpy #EXPTRESHOLD+(ENDFREQTBH-FreqTablePalHi)
        bcs MAXSLID     //IF POINTS OVER TABLE
        cpy #EXPTRESHOLD //DECIDE ON FINE/ROUGH HALF OF THE EXPONENT-TABLE
        bcs calcVib     //if bigger than $60 limit of table, switch to rough table
        lda EXPTBASE,y
wrFmodL: sta vibFreqLo,x
        lda #0          //IN FINE-RANGE FREQ-ADD-HI SHOULD BE 0
        beq wrFmodH     //jump
MAXSLID: ldy #EXPTRESHOLD+(ENDFREQTBH-FreqTablePalHi)
calcVib: lda FreqTablePalLo-EXPTRESHOLD,y

        sta vibFreqLo,x
        lda FreqTablePalHi-EXPTRESHOLD,y //EXPTABH,Y ;EXPONENTIAL TABLE SIMULATES MULTIPLICATION - CALCULATED VIBRATO
wrFmodH: sta vibFreqHi,x

//-------------------------------------------------------------
//write sprite sequence for decay cycle based on difference max vol and sustain val
//attack always goes from 0 to max so that sequence is set
		lda #15
		sec
		sbc sustainTmp
		lsr
		lsr		//now 0,1,2 or 3
		asl
		asl		//now 0,4,8 or 12
		tax
		ldy index9

		lda decayLoudnessTable,x
		sta frames + 4,y
		lda decayLoudnessTable + 1,x
		sta frames + 5,y
		lda decayLoudnessTable + 2,x
		sta frames + 6,y
		lda decayLoudnessTable + 3,x
		sta frames + 7,y

		lda attackTmp
		asl
		asl
		tax
		lda attackLengthPerFrame,x
		sta frameLengths,y 
		lda attackLengthPerFrame + 1,x
		sta frameLengths + 1,y
		lda attackLengthPerFrame + 2,x
		sta frameLengths + 2,y
		lda attackLengthPerFrame + 3,x
		sta frameLengths + 3,y

		lda decayTmp
		asl
		asl
		tax
		lda decayLengthPerFrame,x
		sta frameLengths + 4,y 
		lda decayLengthPerFrame + 1,x
		sta frameLengths + 5,y
		lda decayLengthPerFrame + 2,x
		sta frameLengths + 6,y
		lda decayLengthPerFrame + 3,x
		sta frameLengths + 7,y
// neutral frame at max length		
		lda #$ff
		sta frameLengths + 8,y

// now set last frame based on sustain value
		lda sustainTmp
		lsr
		lsr
		clc
		adc #SUSTAIN_LEVEL_0
		sta frames + 8,y
	
		ldx index1
		lda #0
		sta animationQueuePosition,x
		ldy index9
		lda frames,y
		sta currentFrame,x
		sta baseFrame,x
setSoundEvent:		
		ldx index3
		lda lengthTmp
		sta currentSoundEventQueue0,x
		ldy index7
		ldx instrTmp
		lda waveTable,x
// should work even with 'silent' instrument, that has wave 00 and 01 is also silent
		ora #%00000001
		sta $D404,y


end:	
	rts	
}


handlePreSong0:
//	break()
	doPreSong(0)
handlePreSong1:
	doPreSong(1)
handlePreSong2:
	doPreSong(2)


decayLoudnessTable:
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_3
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_2
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_1
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_0 
	
	
		

animationQueuePosition:	 .byte 0,0,0
animationNrFrames:	 .byte 1,1,1
//a0,a1,a2,a3,a4,s0,s1,s2,s3, neutral	
frameLengths:	
	.byte random()*128+128,0,0,0
	.byte 0,0,0,0
	.byte 255	
	.byte random()*128+64,0,0,0
	.byte 0,0,0,0
	.byte 255	
	.byte random()*128+32,0,0,0
	.byte 0,0,0,0
	.byte 255	

frames:
	.byte SUSTAIN_LEVEL_0, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_3
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_0
	.byte SUSTAIN_LEVEL_0, SLEEPY_FRAME

	.byte SUSTAIN_LEVEL_0, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_3
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_0
	.byte SUSTAIN_LEVEL_0, SLEEPY_FRAME

	.byte SUSTAIN_LEVEL_0, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_3
	.byte SUSTAIN_LEVEL_3, SUSTAIN_LEVEL_2, SUSTAIN_LEVEL_1, SUSTAIN_LEVEL_0
	.byte SUSTAIN_LEVEL_0, SLEEPY_FRAME




FreqTablePalLo:
	        //      C   C#  D   D#  E   F   F#  G   G#  A   A#  B
                .byte $17,$27,$39,$4b,$5f,$74,$8a,$a1,$ba,$d4,$f0,$0e  // 1
                .byte $2d,$4e,$71,$96,$be,$e8,$14,$43,$74,$a9,$e1,$1c  // 2
                .byte $5a,$9c,$e2,$2d,$7c,$cf,$28,$85,$e8,$52,$c1,$37  // 3
                .byte $b4,$39,$c5,$5a,$f7,$9e,$4f,$0a,$d1,$a3,$82,$6e  // 4
                .byte $68,$71,$8a,$b3,$ee,$3c,$9e,$15,$a2,$46,$04,$dc  // 5
                .byte $d0,$e2,$14,$67,$dd,$79,$3c,$29,$44,$8d,$08,$b8  // 6
                .byte $a1,$c5,$28,$cd,$ba,$f1,$78,$53,$87,$1a,$10,$71  // 7
                .byte $42,$89,$4f,$9b,$74,$e2,$f0,$a6,$0e,$33,$20,$ff  // 8

EXPTABH: .byte 0,0,0,0,0,0,0,0, 0,0//LET EXPONENT TABLE BE A BIT MORE COMPLETE (FOR SUBSTRACTING KB.TRACK)
             //USED AS CALCULATED VIBRATO-AMPLITUDE TABLE TOO
        .byte 0         //FOR UNCALCULATED ZERO VIBRATO
FreqTablePalHi:
			   //      C   C#  D   D#  E   F   F#  G   G#  A   A#  B
                .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$02  // 1
                .byte $02,$02,$02,$02,$02,$02,$03,$03,$03,$03,$03,$04  // 2
                .byte $04,$04,$04,$05,$05,$05,$06,$06,$06,$07,$07,$08  // 3
                .byte $08,$09,$09,$0a,$0a,$0b,$0c,$0d,$0d,$0e,$0f,$10  // 4
EXPTAB2:                
               .byte $11,$12,$13,$14,$15,$17,$18,$1a,$1b,$1d,$1f,$20  // 5
                .byte $22,$24,$27,$29,$2b,$2e,$31,$34,$37,$3a,$3e,$41  // 6
                .byte $45,$49,$4e,$52,$57,$5c,$62,$68,$6e,$75,$7c,$83  // 7
                .byte $8b,$93,$9c,$a5,$af,$b9,$c4,$d0,$dd,$ea,$f8,$ff  // 8
ENDFREQTBH:
	//probably not needed
        //.if (feature.CALCVIBRATO_ON+feature.PWKEYBTRACK_ON+feature.FILTKBTRACK_ON+(COMPILEDAPP==1))
        .byte $F9,$FA,$FB,$FC,$FD,$FE,$FF,$FF //EXPAND EXPONENT-TABLE WITH SLOPE FOR KB.TRACKING
        //.fi
ENDEXPTABH:



	ldx #0
!:	lda #0
	sta $0400,x
	sta $0400+250,x
	sta $0400+500,x
	sta $0400+750,x

	lda #CYAN
	sta $d800,x
	sta $d800+250,x
	sta $d800+500,x
	sta $d800+750,x
	inx
	cpx #250
	bne !-
	
    sei
	jsr setupSprites
	jsr setCharSet
	jsr copyValsToZeroPage
	jsr initSound	

// ------------------ intital draw chars -- -----------------	
	ldx #0
	stx $0801	
!:	 
	lda FURB_FRAME_ZP_START,x
	sec
	sbc #SPRITE_START
	cpx #48//64
	beq !+
	jsr copySpriteChars
	inc $0801	
	ldx $0801	
	jmp !-	
!:
	jsr drawLogo
//-------------------------------------------------------
// set initial furby colors
	ldx #0
	stx $0801
loop:
	lda furbyCharColors,x
	sta sm0 + 1
	txa
	asl
	tax
	lda charStart,x
	sta sm1 + 1
	lda charStart + 1,x
	adc #>($D800 - SCREEN_START)
	sta sm1 + 2
	
	ldy #0
!:	lda charOffset,y
	tax
sm0:	
	lda #0
sm1:	
	sta $ffff,x
	iny	
	cpy #9	
	bne !-	

	ldx $0801
	inx
	stx $0801
	cpx #48//64
	beq end 
	jmp loop	
end:	


   
             
             lda #$7f
			sta $dc0d  //disable timer interrupts which can be generated by the two CIA chips
			sta $dd0d  //the kernal uses such an interrupt to flash the cursor and scan the keyboard, so we better stop it.

			lda $dc0d  //by reading this two registers we negate any pending CIA irqs.
			lda $dd0d  //if we don't do this, a pending CIA irq might occur after we finish setting up our irq.
           //we don't want that to happen.

             
             lda #<int
             sta $fffe
             lda #>int
             sta $ffff

             lda #IRQ_LINE
             sta $d012
             lda #D011_BASE
             sta $d011

             lda #$01
             sta $d01a        // enable raster IRQ  

			lda #0
			sta GHOST_BYTE		// we'll be opening the border
			lda #BACKGROUND_COLOR
			sta $d021
			lda #ORANGE
			sta $d020
			
             lda #$35		// turn off basic and kernal
             sta $01
             cli
			jmp *	

setCharSet:
	//write to IO where charset resides
		lda $d018
		and #%11110001
		ora #%00001000

		sta	$d018
	rts

copyValsToZeroPage:
	ldx #48
!:
//!:	txa
//	and #$01
//	clc
//	adc #SLEEP_FRAME_0 + SPRITE_START
	lda #SLEEP_FRAME_0 + SPRITE_START
	sta FURB_FRAME_ZP_START,x
	dex
	bpl !-
	rts


.macro doVibrato(index) {
//VIBFREQU = lo nybble (03)
//AMPLITUDE  = hi hybble (08)
	//index is voice index
NORMVIB: ldy vibDelay + index //VIDELCNT + index  //HANDLE VIBRATO-DELAY IN CASE OF NORMAL VIBRATOS
        bmi DOVIBRA     //IF DELAY-COUNTER FINISHED
        dec vibDelay + index //VIDELCNT + index
        lda #0
        sta doVib + index
        jmp ENDVIB
DOVIBRA: 
		ldx baseFrame+index // to sync vibrato with animation
		lda vibCount + index//VIBRACNT+index
        bne decVcnt     //IF NOT ZERO (COUNTED DOWN) SIMPLY SUBSTRACT 1 (OR NOTHING, IF SLIDE)
        lda vibFreq + index //VIBFREQU+index
decVcnt: sec
        sbc #1
        sta vibCount + index //VIBRACNT+index
        asl             //IF HALF OF COUNTER REACHED, CHANGE VIBRATO-SLIDE-DIRECTION
        cmp vibFreq + index //VIBFREQU+index  //SET CARRY BIT BASED ON WHICH HALF THE FREQUENCY COUNTER IS IN
        bcc ADDFREQ     //CARRY IS SET HERE CORRECTLY, NO NEED TO INITIALIZE IN NEXT STEP
SUBFREQ: lda #0 
 		sta vibUp + index 
		lda freqLo + index //FREQLO+index
        sbc vibFreqLo + index //FREQMODL+index  //SUBSTRACT LOW-BYTE PART, SET CARRY-BIT
        sta freqLo + index //
        sta $D400 + index*7
        lda freqHi + index //
        sbc vibFreqHi + index ////FREQMODH+index  //SUBSTRACT HIGH-BYTE PART AND CARRY-BIT
        jmp STORVHI
ADDFREQ: lda #1 
 		sta vibUp + index
 		inx
		lda freqLo + index //FREQLO+index    //PRE-FETCH DATA
        adc vibFreqLo + index //FREQMODL+index  //ADD LOW-BYTE PART, SET CARRY-BIT
        sta freqLo + index //FREQLO+index
        sta $D400 + index*7
        lda freqHi + index //FREQHI+index
        adc vibFreqHi + index //FREQMODH+index  //ADD HIGH-BYTE PART AND CARRY-BIT
STORVHI: sta freqHi + index //
		sta $D401 + index*7
		stx currentFrame + index
ENDVIB:
}
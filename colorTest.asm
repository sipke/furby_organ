.var brkFile = createFile("debug.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 "setup" 

	sei
             lda #<int
             sta $0314
             lda #>int
             sta $0315

             lda #$ff
             sta $d012
             //lda $d011		// bit 8 =0
             //and #254
             lda #$1b
             sta $d011

             lda #$01
             sta $d01a        // enable raster IRQ  

	cli			
	ldx #0
	lda #160
!:	sta $0400,x
	sta $0400+250,x
	sta $0400+500,x
	inx
	bne !-
	jmp *			

/*------------------------------------------------------------------------
		ALSO look at 
		http://www.aaronbell.com/secret-colours-of-the-commodore-64/
		or:
		https://ilesj.wordpress.com/2016/03/30/old-vic-ii-colors-and-color-blending/
	*/			
int:				
	lda $d019				
	and #%0000001				
	bne !+				
	jmp $ea31				
!:					
	inc $d020				
	
	lda eorVal					
	bne !+					
	jmp eorIs0					
!:	jmp eorIs1						
					
					

end:					
	lda eorVal					
	eor #1					
	sta eorVal					
	dec $d020				
	lsr $d019				
	jmp $ea81				

.var cs= List().add(BLACK,BLUE,BROWN,DARK_GREY,GREEN,RED,LIGHT_BLUE,GREY,PURPLE,ORANGE,CYAN,LIGHT_GREY,LIGHT_RED,LIGHT_GREEN,YELLOW,WHITE) 

eorIs0:
	.for (var y= 0; y<16; y++){
		
		.if (y==0) {
			.for (var x= 0; x<16; x++){
				lda #cs.get(x)
				sta $d800+40*y+x
			}
		}else {
			lda #cs.get(y)
			.for (var x= 0; x<16; x++){
				sta $d800+40*y+x	
			}
		}
	}
	jmp end
eorIs1:
	.for (var y= 0; y<17; y++){
		.for (var x= 0; x<16; x++){
			lda #cs.get(x)
			sta $d800+40*y+x
		}
	}
	jmp end
eorVal:	.byte 0					

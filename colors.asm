.const COLS_BY_LUMINANCE = List().add(BLUE,BROWN,RED,DARK_GREY,PURPLE,ORANGE,LIGHT_BLUE,GREY,GREEN,LIGHT_RED,CYAN,LIGHT_GREY,YELLOW,LIGHT_GREEN)
.const COLS_NAME = List().add("BLACK", "WHITE", "RED", "CYAN", "PURPLE", "GREEN", "BLUE", "YELLOW", "ORANGE", "BROWN", "LIGHT_RED", "DARK_GREY", "GREY", "LIGHT_GREEN", "LIGHT_BLUE", "LIGHT_GREY")
.struct ColorPair {c1, c2}

.var colorPairs= List()
.var c1
.var c2

.var colorFile = createFile("colors.txt") 

.import source "colors_1.txt"

.for(var i=0;i<48;i++) {
	.eval c1=floor(random()*6)
	.eval c2=c1 + 5 + floor(random()*4)
	.if(c2==12) .eval c2=10		// don't use yellow for fur, because beak is also yellow
	.eval colorPairs.add(ColorPair(COLS_BY_LUMINANCE.get(c1),COLS_BY_LUMINANCE.get(c2)))
	.eval colorFile.writeln(" .eval colorPairs.add(ColorPair("+ COLS_NAME.get(colorPairs.get(colorPairs.size()-1).c1)+ "," + COLS_NAME.get(colorPairs.get(colorPairs.size()-1).c2)+ "))")
	//.print "pair:"  + c1 + "," + c2 + "=" +colorPairs.get(colorPairs.size()-1).c1 + ","+colorPairs.get(colorPairs.size()-1).c2  
}



/*
.for (var i=0; i<24;i++) {
	.eval colorPairs.add(ColorPair(BLACK,WHITE))
	}
.for (var i=24; i<48;i++) {
	.eval colorPairs.add(ColorPair(WHITE,BLACK))
	}
.for (var i=47;i>=0; i--) {
	.print "pair:"  + COLS_NAME.get(colorPairs.get(i).c1) + ","+COLS_NAME.get(colorPairs.get(i).c2)
}
*/


/*			CHRISTMAS VERSION
.const COLS_BY_LUMINANCE = List().add(RED, ORANGE, LIGHT_RED, GREEN, LIGHT_GREEN)
.struct ColorPair {c1, c2}

.var colorPairs= List()
.var c1
.var c2
.for(var i=0;i<48;i++) {
	.eval c1=floor(random()*3)
	.eval c2=3 + floor(random()*2)
	.eval colorPairs.add(ColorPair(COLS_BY_LUMINANCE.get(c1),COLS_BY_LUMINANCE.get(c2)))
}
*/
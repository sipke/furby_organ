.macro calcvx() {
// pseudo code	
//        If (xc - x) < 0 Then
	lda x
	cmp xc
	bcc xsmaller
	bne !+
	lda x+1
	cmp xc+1
	bcc xsmaller
//            dx = (x - xc) / 8
!:	lda x + 1	
	sec	
	sbc xc +1	
	sta dx +1	
	lda x	
	sbc xc	
	sta dx	
		
	lsr dx	
	ror dx+1	
	lsr dx	
	ror dx+1	
	lsr dx	
	ror dx+1	
	lsr dx	
	ror dx+1	
		
//            vx = vx - dx	
	lda vx + 1
	sec
	sbc dx + 1
	sta vx + 1	
	lda vx 
	sbc dx 
	sta vx	
	jmp endmacro
//        Else
xsmaller:
//            dx = (xc - x) / 8
	lda xc	+ 1
	sec	
	sbc x +1	
	sta dx +1	
	lda xc	
	sbc x 	
	sta dx	

	lsr dx	
	ror dx+1	
	lsr dx	
	ror dx+1	
	lsr dx	
	ror dx+1	
	lsr dx	
	ror dx+1	
	
//            vx = vx + dx
	lda vx + 1
	clc
	adc dx + 1
	sta vx +1
	lda vx 
	adc dx 
	sta vx 
endmacro:
}
.macro calcvy() {
//        If (yc - y) < 0 Then
	lda y
	cmp yc
	bcc ysmaller
	bne !+
	lda y+1
	cmp yc+1
	bcc ysmaller

//            dy = (y - yc) / 16
!:	lda y + 1	
	sec	
	sbc yc + 1	
	sta dy + 1	
	lda y	
	sbc yc	
	sta dy	

	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
//            vy = vy - dy
	lda vy + 1	
	sec	
	sbc dy + 1	
	sta vy + 1	
	lda vy 	
	sbc dy 	
	sta vy 	
	jmp endmacro	
	
//      Else
ysmaller:
//        dy = (yc - y) / 16
!:	lda yc + 1	
	sec	
	sbc y + 1	
	sta dy + 1	
	lda yc	
	sbc y 
	sta dy	

	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
	lsr dy	
	ror dy+1	
		
//      vy = vy + dy
	lda vy + 1	
	clc	 
	adc dy + 1	
	sta vy +1	
	lda vy 	
	adc dy 	
	sta vy 	
endmacro:	
}
.macro calcPos(){
//        x = x + vx
	lda x + 1
	clc
	adc vx + 1
	sta x + 1
	lda x
	adc vx
	sta x
//        xsmall = x / 256
	lda x
	clc
	adc #32
	sta $d00e

//        y = y + vy	
	lda y + 1
	clc
	adc vy + 1
	sta y + 1
	lda y
	adc vy
	sta y
//        ysmall = y / 256
	lda y
	clc
	adc #112
	sta $d00f
	
	//break()
	lda vy
	and #%10000000
	tax
	cmp vysign
	beq !+

	lda d01b
	eor #%10000000
	sta d01b
!:	stx vysign	
}


x: .byte 13,23	
y: .byte 4,127	
dx:	.word 0	
dy:	.word 0	
vx: .byte -1,127	
vy: .byte 1,73	
xc:	.byte 7,128	
yc: .byte 7,128	
vysign:	.byte 0
changeOfDirection:	.byte 0
d01b:	.byte 0
